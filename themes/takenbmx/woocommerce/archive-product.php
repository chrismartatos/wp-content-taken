<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;


//do_action( 'woocommerce_before_main_content' );
?>
<?php //get_template_part('templates/shop-header');?>

  <div class="container">
    <div class="row">

     <div class="col-xs-12 col-md-3 col-lg-3" role="sidebar-categories">
       <div class="wrap-products-cat-nav">
         <h3>Categories</h3>

         <div id="toggle-mobile-menu">
           <div class="visible-sm visible-xs toggle-title">
             Categories
             <span class="open-ar fa fa-caret-right"></span>
           </div>

           <nav id="products-cat-nav" class="mobile-menu toggle-menu">
             <?php
               wp_nav_menu([
                 'theme_location' => 'store_navigation',
               ]);
             ?>

             <div class="cyprus-banner">
	           <img src="https://www.takenbmx.gr/wp-content/uploads/2021/09/cyprusbanner-e1632737096852.png" alt="TakenBmx Cyprus">
	       </div>
           </nav>
         </div>
       </div>

     </div><!--END role sidebar-->

     <div class="col-xs-12 col-md-9 col-lg-9" role="categories-content">
       <div class="woo-content clearfix">
         <?php
         //Show categories - with filters
         if(is_shop()):
           get_template_part('templates/woocommerce-elements/archive-products-landing');
         else:
           get_template_part('templates/woocommerce-elements/archive-product-categories');
         endif;
         ?>
       </div><!--END .woo-content-->
     </div><!--END role categories-content-->

   </div><!--END .row-->
 </div><!--END .container-->
<?php
do_action( 'woocommerce_after_main_content' );
?>
