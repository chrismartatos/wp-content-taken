<ul class="woocommerce-mini-cart cart_list color-wrapper product_list_widget <?php //echo esc_attr( $args['list_class'] ); ?>">
  <?php
  do_action( 'woocommerce_before_mini_cart_contents' );
  foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item )
  {
    $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) )
    {

      //$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
      $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
      $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
      $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
      ?>
      <li class="woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">

        <div class="cart-item-wrap">
          <div class="col-left">
            <?php if ( empty( $product_permalink ) ) : ?>
              <?php echo $thumbnail; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
            <?php else : ?>
              <a href="<?php echo esc_url( $product_permalink ); ?>">
                <?php echo $thumbnail; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
              </a>
            <?php endif; ?>
          </div>

          <div class="col-right">
            <a href="<?php echo esc_url( $product_permalink ); ?>">
              <h5 class="title item-color"><?php echo $_product->get_name(); ?></h5>
            </a>
            <div class="item-color price-item"><?php echo $product_price; ?></div>
            <div class="item-color">
              <?php
              //Get space cart data from hook price
              echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
              ?>
            </div>
          </div>

          <div class="col-qty">
            <?php
            echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="qty-text hide-on-sm item-color">' . sprintf( 'Ποσ. %s', $cart_item['quantity'] ) . '</span>', $cart_item, $cart_item_key );
           ?>
          </div>

          <div class="col-delete">
            <?php
            echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
              'woocommerce_cart_item_remove_link',
              sprintf(
                '<a href="%s" class="remove-item item-color remove_from_cart_button" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s">&times;</a>',
                esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                esc_attr__( 'Remove this item', 'woocommerce' ),
                esc_attr( $product_id ),
                esc_attr( $cart_item_key ),
                esc_attr( $_product->get_sku() )
              ),
              $cart_item_key
            );
            ?>
          </div>
        </div><!-- .cart-item-wrap-->
      </li>
      <?php
    }
  }
  do_action( 'woocommerce_mini_cart_contents' );
  ?>
</ul>
