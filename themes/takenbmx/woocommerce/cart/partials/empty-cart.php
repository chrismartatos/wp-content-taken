
<!-- Empty Message -->
<li class="empty">
  <?php
  //IF cart is NOT empty
  if ( ! WC()->cart->is_empty() ) :
  ?>
    <div class="empty-cart-msg">

    </div>
  <?php else: //Empty cart  ?>
    <div class="empty-cart-msg">
      <?php the_field('mini_cart_empty_message','option'); ?>
    </div>
  <?php
  endif;
  ?>
</li>
