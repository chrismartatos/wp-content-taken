<div class="total">
  <span class="label"><?php _e( 'ΣΥΝΟΛΟ', 'woocommerce' ); ?>:</span>
  <span id="get-price" class="price"><?php echo WC()->cart->get_cart_subtotal(); ?></span>
</div>

<nav class="cart-review-order">
  <a href="<?php echo esc_url( wc_get_cart_url() );?>" class="taken-btn cart-button btn button white-button">
    <?php esc_html_e( 'ΔΕΣ ΤΟ ΚΑΛΑΘΙ', 'woocommerce' ); ?>
  </a>
</nav>

<?= get_field('mini_cart_free_shipping','option'); ?>
