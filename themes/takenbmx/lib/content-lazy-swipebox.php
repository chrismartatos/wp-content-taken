<?php
/*-------------------------------------------------------------------------------------
       Lazyload
-------------------------------------------------------------------------------------*/
add_filter('the_content', 'filter_lazyload');

function filter_lazyload($content)
{
    return preg_replace_callback('/(<\s*img[^>]+)(src\s*=\s*"[^"]+")([^>]+>)/i', 'preg_lazyload', $content);
}

function preg_lazyload($img_match)
{
    $img_replace = $img_match[1] . ' data-original' . substr($img_match[2], 3) . $img_match[3];

    $img_replace = preg_replace('/class\s*=\s*"/i', 'class="lazyload-init ', $img_replace);

    $img_replace .= '<noscript>' . $img_match[0] . '</noscript>';

    return $img_replace;
}



/*----------------------------------------------------------
		Customize links
----------------------------------------------------------*/
function add_class_to_image_links($html, $attachment_id, $attachment)
{
    $linkptrn = "/<a[^>]*>/";
    $found = preg_match($linkptrn, $html, $a_elem);

    // If no link, do nothing
    if($found <= 0) return $html;

    $a_elem = $a_elem[0];

    // Check to see if the link is to an uploaded image
    $is_attachment_link = strstr($a_elem, "wp-content/uploads/");

    // If link is to external resource, do nothing
    if($is_attachment_link === FALSE) return $html;

    if(strstr($a_elem, "class=\"") !== FALSE)
    { // If link already has class defined inject it to attribute
        $a_elem_new = str_replace("class=\"", "class=\"swipebox img-item", $a_elem);
        $html = str_replace($a_elem, $a_elem_new, $html);
    }else{ // If no class defined, just add class attribute
        $html = str_replace("<a ", "<a  rel=\"image\" class=\"swipebox img-item\" ", $html);
    }

    return $html;
}

add_filter('image_send_to_editor', 'add_class_to_image_links', 10, 3);
