<?php

function mobile_menu() {

  register_nav_menus(array(
  'mobile_nav' => __( 'Mobile Menu', 'whisk' )
  ));
  }

add_action( 'after_setup_theme', 'mobile_menu' );


$logoPos = get_theme_mod('logo_position', 'whisk-navbar-left');

if($logoPos == 'branding-center'){

function center_left_menu() {

  register_nav_menus(array(
  'left_nav' => __( 'Left Menu', 'whisk' )
  ));
  }
add_action( 'after_setup_theme', 'center_left_menu' );

function center_right_menu() {

  register_nav_menus(array(
  'right_nav' => __( 'Right Menu', 'whisk' )
  ));
  }
add_action( 'after_setup_theme', 'center_right_menu' );

}

 class Split_Menu_Walker extends Walker_Nav_Menu {

    var $current_menu = null;
    var $break_point  = 0;

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
  		$indent = str_repeat( "\t", $depth );
  		$output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\">\n";
  	}


    function start_el(&$output, $item, $depth = 0, $args = array(), $id=0) {
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        global $wp_query;



/* added code */

        if( !isset( $this->current_menu ) )
            $this->current_menu = wp_get_nav_menu_object( $args->menu );

        if($depth == 0){
          if( !isset( $this->break_point ) )
              $this->break_point = ceil( $this->current_menu->count / 2 ) + 1;
        }


   		if ( strcasecmp( $item->attr_title, 'divider' ) == 0 && $depth === 1 ) {
   			$output .= $indent . '<li role="presentation" class="divider">';
   		} else if ( strcasecmp( $item->title, 'divider') == 0 && $depth === 1 ) {
   			$output .= $indent . '<li role="presentation" class="divider">';
   		} else if ( strcasecmp( $item->attr_title, 'dropdown-header') == 0 && $depth === 1 ) {
   			$output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
   		}  else if ( strcasecmp($item->attr_title, 'disabled' ) == 0 ) {
   			$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
   		} else {
   			$class_names = $value = '';
   			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
   			$classes[] = 'menu-item-' . $item->ID;

         if($depth == 0){
           $classes[] = 'lvl-1';
         }
         if($depth == 1){
           $classes[] = 'lvl-2';
         }

   			/*$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

        if ( $args->has_children )
          $class_names .= ' dropdown';*/

          $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
          if ( $args->has_children )
            $class_names .= ' dropdown';


    /* added code */

      //  $class_names = $value = '';

        //$classes = empty( $item->classes ) ? array() : (array) $item->classes;
      //  $classes[] = 'menu-item-' . $item->ID;

      //  $class_names = join( ' ', apply_filters( 'nav_menu_css_class ', array_filter( $classes ), $item, $args ) );

        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

        $image = attachment_url_to_postid(get_theme_mod( 'logo_image' ));
        $logo_src = wp_get_attachment_image_src($image, 'logo')[0];

        $sticky = attachment_url_to_postid(get_theme_mod( 'sticky_logo_image' ));
        $sticky_logo = wp_get_attachment_image_src($sticky, 'logo')[0];

        $logo_padding = get_theme_mod('logo_padding');

        $blogName = get_bloginfo( 'name' );

        if( $this->break_point == $item->menu_order )
            $output .= $indent . '</li></ul><ul class="nav navbar-nav nav-right"><li' . $id . $value . $class_names .'>';
        else
            $output .= $indent . '<li' . $id . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
  }//added


}//end class
