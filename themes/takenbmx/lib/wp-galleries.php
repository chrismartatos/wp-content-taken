<?php

/*-------------------------------------------------------------------------------
	  Wordpress add and customize Galleries
-------------------------------------------------------------------------------*/
add_filter('post_gallery','wordpress_custom_gallery',10,2);

function wordpress_custom_gallery($string,$attr)
{
	$type = $attr['theme_gallery_type']; /* Matching select */
	$posts = get_posts(array('include' => $attr['ids'],'post_type' => 'attachment', 'orderby' => 'post__in'));
	$output = '';

	if($type == 'slideshow')
	{
		$output .= '<div class="simple-gallery slideshow">';

		foreach($posts as $imagePost)
		{
			$id = $imagePost->post_parent;
			$caption = $imagePost->post_excerpt;
      $img_t = wp_get_attachment_image_src($imagePost->ID, 'thumbnail');
      $img_m = wp_get_attachment_image_src($imagePost->ID, 'medium');
			$img_l = wp_get_attachment_image_src($imagePost->ID, 'large');
      $img_f = wp_get_attachment_image_src($imagePost->ID, 'full');

			$output .= '<div class="slide">';
      $output .= '<a class="item" href="'.$img_l[0].'" title="'.$caption.'" rel="image"><img alt="'.$caption.'" src="'.$img_l[0].'"/></a>';
      $output .= '</div>';
		}

		$output .= '</div>';
	}
	else if($type == 'carousel')
	{
    $output .= '<div class="simple-gallery carousel-slideshow">';

    foreach($posts as $imagePost)
		{
			$id = $imagePost->post_parent;
			$caption = $imagePost->post_excerpt;
      $img_t = wp_get_attachment_image_src($imagePost->ID, 'thumbnail');
      $img_m = wp_get_attachment_image_src($imagePost->ID, 'medium');
      $img_l = wp_get_attachment_image_src($imagePost->ID, 'large');
      $img_f = wp_get_attachment_image_src($imagePost->ID, 'full');

      $output .= '<div class="slide">';
      $output .= '<a class="item" href="'.$img_l[0].'" title="'.$caption.'" rel="image"><img alt="'.$caption.'" src="'.$img_l[0].'"/></a>';
      $output .= '</div>';
		}

		$output .= '</div>';
	}

	return $output;
}


/* Register select function
-------------------------------------------------------------------------------*/
add_action('print_media_templates', 'wordpress_galleries_select');

function wordpress_galleries_select()
{
	// the "tmpl-" prefix is required,
	// and your input field should have a data-setting attribute
	// matching the shortcode name
	?>
	<script type="text/html" id="tmpl-wp-custom-gallery-setting">
		<label class="setting">
			<span><?php _e('Gallery Type'); ?></span>
			<select data-setting="theme_gallery_type">
				<option value="default">Default Wordpress Gallery</option>
				<option style="display:none;" value="slideshow">Simple Slideshow</option>
				<option style="display:none;" value="carousel">Carousel Slideshow</option>
			</select>
		</label>
	</script>

	<script>
		jQuery(document).ready(function()
		{
			// add your shortcode attribute and its default value to the
			// gallery settings list; $.extend should work as well...
			_.extend(wp.media.gallery.defaults, {
				theme_gallery_type: 'default'
			});

			// merge default gallery settings template with yours
			wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
				template: function(view)
				{
					return wp.media.template('gallery-settings')(view)
							 + wp.media.template('wp-custom-gallery-setting')(view);
				}
			});
		});
	</script>
	<?php
}
