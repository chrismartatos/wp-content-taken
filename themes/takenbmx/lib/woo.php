<?php


/*-------------------------------------------------------------------
   Cart coupon text
-------------------------------------------------------------------*/
add_action( 'woocommerce_cart_coupon', 'action_woocommerce_cart_coupon', 10, 0 );

function action_woocommerce_cart_coupon()
{
  $coupon_text = get_field('mini_cart_free_shipping','option');

  if(!empty($coupon_text)):
    echo '<div style="margin: 1rem 0 0 0;">';
    echo $coupon_text;
    echo '</div>';
  endif;
};



/*-------------------------------------------------------------------
   Woo mini cart ajax
-------------------------------------------------------------------*/
function mode_theme_update_mini_cart() {
  echo wc_get_template( 'cart/mini-cart.php' );
  die();
}
add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );

/*-------------------------------------------------------------------
   Placeholders
-------------------------------------------------------------------*/
add_filter('woocommerce_default_address_fields', 'override_address_fields');

function override_address_fields( $address_fields )
{
  $address_fields['address_1']['placeholder'] = 'Οδός';
  $address_fields['address_2']['placeholder'] = 'Διαμέρισμα, όροφος';


  return $address_fields;
}

/*-------------------------------------------------------------------
   Seperator
-------------------------------------------------------------------*/
add_filter( 'woocommerce_breadcrumb_defaults', 'wps_breadcrumb_delimiter' );
function wps_breadcrumb_delimiter( $defaults ) {
  $defaults['delimiter'] = ' <span class="seperator fa fa-chevron-right" style="font-size: 11px;"></span> ';
  return $defaults;
}

/*-------------------------------------------------------------------
   Shipping text
-------------------------------------------------------------------*/
add_filter( 'woocommerce_shipping_package_name', 'custom_shipping_package_name' );
function custom_shipping_package_name( $name ) {
  return 'Μεταφορικά';
}

/*-------------------------------------------------------------------
   Shipping
-------------------------------------------------------------------*/
add_action( 'woocommerce_after_cart_totals', 'shipping_message');

function shipping_message()
{
  $elta_msg = '<div class="taken-cart-msg">'.get_field('shipping_message','option').'</div>';

  echo $elta_msg;
}


/*-------------------------------------------------------------------
   Single product
-------------------------------------------------------------------*/
add_action( 'woocommerce_after_add_to_cart_form', 'single_product_shipping_info');

function single_product_shipping_info()
{
  $elta_msg = '<div class="taken-cart-msg">'.get_field('shipping_message','option').'</div>';

  echo $elta_msg;
}


 /*-------------------------------------------------------------------
    Shipping Only display the most expensive shipping rate
 -------------------------------------------------------------------*/
function my_only_show_most_expensive_shipping_rate( $rates, $package ) {

	$most_expensive_method = '';

	// Loop through shipping rates
	if ( is_array( $rates ) ) :
		foreach ( $rates as $key => $rate ) :

			// Set variables when the rate is more expensive than the one saved
			if ( empty( $most_expensive_method ) || $rate->cost > $most_expensive_method->cost ) :
				$most_expensive_method = $rate;
			endif;

		endforeach;
	endif;

	// Return the most expensive rate when possible
	if ( ! empty( $most_expensive_method ) ) :
		return array( $most_expensive_method->id => $most_expensive_method );
	endif;

	return $rates;

}
add_action( 'woocommerce_package_rates', 'my_only_show_most_expensive_shipping_rate', 10, 2 );


/*-------------------------------------------------------------------
  Breadcrumb
-------------------------------------------------------------------*/
add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );

function woo_custom_breadrumb_home_url() {
  $products = get_home_url().'/products';

  return $products;
}

//Shop
add_filter( 'woocommerce_breadcrumb_defaults', 'woo_change_breadcrumb_home_text' );

function woo_change_breadcrumb_home_text( $defaults ) {
	$defaults['home'] = 'Products';

	return $defaults;
}


/*-------------------------------------------------------------------
  Edit my account menu order
-------------------------------------------------------------------*/
function my_account_menu_order($menu_links)
{
  unset( $menu_links['downloads'] );

	$menuOrder = array(
		'dashboard'          => __( 'Dashboard', 'woocommerce' ),
		'orders'             => __( 'Orders', 'woocommerce' ),
		'edit-address'       => __( 'Addresses', 'woocommerce' ),
		'edit-account'    	 => __( 'My Account', 'woocommerce' ),
		'customer-logout'    => __( 'Logout', 'woocommerce' ),
	);
	return $menuOrder;
}
add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );


/*-------------------------------------------------------------------
  Thank you message
-------------------------------------------------------------------*/
add_filter('woocommerce_thankyou_order_received_text', 'woo_change_order_received_text', 10, 2 );

function woo_change_order_received_text( $str, $order ) {

  $acf_thankyou = get_field('thank_you_message','option');

  $new_str =  (!empty($acf_thankyou))? '<div class="taken-thank-you">'.$acf_thankyou.'</div>': '';

  return $new_str;
}


/*-------------------------------------------------------------------
  Excerpt
-------------------------------------------------------------------*/
function new_wp_trim_excerpt($text)
{
    $raw_excerpt = $text;
    if ( '' == $text ) {
      $text = get_the_content('');

      $text = strip_shortcodes( $text );

      $text = apply_filters('the_content', $text);
      $text = str_replace(']]>', ']]>', $text);

      $text = strip_tags($text, '<blockquote><q><cite><td><a><p><br><ol><ul>
      <li>');
      $excerpt_length = apply_filters('excerpt_length', 55);

      $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
      $words = preg_split('/[\n|\r|\t|\s]/', $text, $excerpt_length + 1,
       PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE );
    if ( count($words) > $excerpt_length ) {
        array_pop($words);
        $text = implode(' ', $words);
        $text = $text . $excerpt_more;
    } else {
        $text = implode(' ', $words);
    }
}
return apply_filters('new_wp_trim_excerpt', $text, $raw_excerpt);

}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'new_wp_trim_excerpt');


 /*-------------------------------------------------------------------
   free_shipping Hide shipping rates when free shipping is available
 -------------------------------------------------------------------*/
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();

	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}

	return ! empty( $free ) ? $free : $rates;
}

//add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );





/*-------------------------------------------------------------------
  Remove elements
-------------------------------------------------------------------*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

// Remove the product description Title
add_filter( 'woocommerce_product_description_heading', '__return_null' );



/*-------------------------------------------------------------------
  Add desc under price
-------------------------------------------------------------------*/
function woocommerce_template_product_description()
{
  wc_get_template( 'single-product/tabs/description.php' );
}

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );


/*-------------------------------------------------------------------
  single products
-------------------------------------------------------------------*/
add_action( 'woocommerce_before_single_product', 'my_product_before_single' );
add_action( 'woocommerce_after_single_product', 'my_product_after_single' );

function my_product_before_single() {
  echo '<div class="container">';
}

function my_product_after_single() {
  echo '</div>';
}


//single summary
add_action( 'woocommerce_before_single_product_summary', 'my_product_before_summary' );
add_action( 'woocommerce_after_single_product_summary', 'my_product_after_summary' );

function my_product_before_summary() {
  echo '<div class="clearfix">';
}

function my_product_after_summary() {
  echo '</div>';
}


/*------------------------------------------------------------
  Image slideshow thumbnails
-------------------------------------------------------------*/
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size )
{
  return array(
    'width' => 180,
    'height' => 180,
    'crop' => 0,
  );
});


/*------------------------------------------------------------
  AJAX call for cart items
-------------------------------------------------------------*/
add_action('wp_ajax_cart_count_retriever', 'cart_count_retriever');
add_action('wp_ajax_nopriv_cart_count_retriever', 'cart_count_retriever');

function cart_count_retriever()
{
    global $wpdb;

    echo WC()->cart->get_cart_contents_count();

    wp_die();
}



/* Billing forms, Registration, Login etc.
===============================================================================*/
function wc_billing_field_strings( $translated_text, $text, $domain )
{
  //print_r($translated_text);
    switch ( $translated_text )
    {

        case 'Enter your address to view shipping options.' : $translated_text = __( 'Εισάγετε τα στοιχεία της διεύθυνση σας.', 'woocommerce' ); break;
        case 'optional' : $translated_text = __( 'προαιρετικό', 'woocommerce' ); break;
        case 'First name' : $translated_text = __( 'Όνομα', 'woocommerce' ); break;
        case 'Last name' : $translated_text = __( 'Επίθετο', 'woocommerce' ); break;
        case 'Company name' : $translated_text = __( 'Επωνυμία εταιρείας', 'woocommerce' ); break;
        case 'Street address' : $translated_text = __( 'Διεύθυνση', 'woocommerce' ); break;
        case 'Country' : $translated_text = __( 'Χώρα', 'woocommerce' ); break;
        case 'Country / Region' : $translated_text = __( 'Χώρα', 'woocommerce' ); break;
        case 'Town / City' : $translated_text = __( 'Πόλη', 'woocommerce' ); break;
        case 'State / County' : $translated_text = __( 'Νομός / Περιφέρεια', 'woocommerce' ); break;
        case 'Postcode / ZIP' : $translated_text = __( 'Ταχυδρομικός κώδικας', 'woocommerce' ); break;
        case 'Phone' : $translated_text = __( 'Τηλέφωνο', 'woocommerce' ); break;
        case 'Apartment, suite, unit etc. (optional)' : $translated_text = __( 'Αριθμός διαμέρισματος', 'woocommerce' ); break;
        case 'Order received' : $translated_text = __( 'Ευχαριστούμε για το αίτημα προσφοράς σας', 'woocommerce' ); break;
        case 'Return to shop' : $translated_text = __( 'ΠΡΟΙΟΝΤΑ', 'woocommerce' ); break;
        case 'Billing details' : $translated_text = __( 'Στοιχεία Αποστολής', 'woocommerce' ); break;
        case 'Your order' : $translated_text = __( 'Η παραγγελία σας', 'woocommerce' ); break;
        case 'Cart totals' : $translated_text = __( 'Συνολο Παραγγελιας', 'woocommerce' ); break;
        case 'Add to cart' : $translated_text = __( 'Προσθηκη στο καλαθι', 'woocommerce' ); break;
        case 'Proceed to checkout' : $translated_text = __( 'ΣΥΝΕΧΕΙΑ ΠΑΡΑΓΓΕΛΙΑΣ', 'woocommerce' ); break;
        case 'Subtotal' : $translated_text = __( 'Σύνολο', 'woocommerce' ); break;
        case 'Shipping' : $translated_text = __( 'Μεταφορικά', 'woocommerce' ); break;
        case 'Total' : $translated_text = __( 'Τελικό σύνολο (με Φ.Π.Α)', 'woocommerce' ); break;
        case 'Update cart' : $translated_text = __( 'Ενημερωση', 'woocommerce' ); break;
        case 'Price' : $translated_text = __( 'Τιμή', 'woocommerce' ); break;
        case 'Product' : $translated_text = __( 'Προϊόντα', 'woocommerce' ); break;
        case 'Place order' : $translated_text = __( 'ΟΛΟΚΛΗΡΩΣΗ ΠΑΡΑΓΓΕΛΙΑΣ', 'woocommerce' ); break;
        case 'Shipping to' : $translated_text = __( 'Αποστολή σε', 'woocommerce' ); break;
        case 'Order notes' : $translated_text = __( 'Σημειώσεις', 'woocommerce' ); break;
        case 'Notes about your order, e.g. special notes for delivery.' : $translated_text = __( 'Οδηγίες και σημειώσεις σχετικά με την παραγγελία σας.', 'woocommerce' ); break;



        //Cart
        case 'Quantity' : $translated_text = __( 'Ποσότητα', 'woocommerce' ); break;
        case 'Your cart is currently empty.' : $translated_text = __( 'Το καλάθι είναι άδειο.', 'woocommerce' ); break;
        case 'Cart updated.' : $translated_text = __( 'Το καλάθι ανανεώθηκε.', 'woocommerce' ); break;
        case 'Shipping costs updated.' : $translated_text = __( 'H διεύθυνση ανανεώθηκε.', 'woocommerce' ); break;

        // Account
        case 'Display name' : $translated_text = __( 'Όνομα χρήστη', 'woocommerce' ); break;
        case 'Current password (leave blank to leave unchanged)' : $translated_text = __( 'Υπάρχων κωδικός (αφήστε το πεδίο κένο αν δεν κάνετε αλλαγή)', 'woocommerce' ); break;
        case 'New password (leave blank to leave unchanged)' : $translated_text = __( 'Καινούργιος κωδικός (αφήστε το πεδίο κένο αν δεν κάνετε αλλαγή)', 'woocommerce' ); break;
        case 'Confirm new password' : $translated_text = __( 'Επιβεβαίωση καινούργιου κωδικού', 'woocommerce' ); break;
        case 'Password change' : $translated_text = __( 'Αλλαγή κωδικού', 'woocommerce' ); break;
        case 'Register' : $translated_text = __( 'Δημιουργία λογαριασμού', 'woocommerce' ); break;
        case 'Log in' : $translated_text = __( 'Είσοδος', 'woocommerce' ); break;
        case 'Login' : $translated_text = __( 'Είσοδος', 'woocommerce' ); break;
        case 'Register' : $translated_text = __( 'Δημιουργία λογαριασμού', 'woocommerce' ); break;
        case 'Password' : $translated_text = __( 'Κωδικός', 'woocommerce' ); break;
        case 'Remember me' : $translated_text = __( 'Να με θυμάσαι', 'woocommerce' ); break;
        case 'Lost your password?' : $translated_text = __( 'Δεν θυμάμαι τον κωδικό μου.', 'woocommerce' ); break;
        case 'Username or email address' : $translated_text = __( 'E-mail', 'woocommerce' ); break;
        case 'Email address' : $translated_text = __( 'E-mail', 'woocommerce' ); break;
        case 'Save changes' : $translated_text = __( 'Ενημέρωση', 'woocommerce' ); break;
        case 'Reset password' : $translated_text = __( 'Αλλαγή κωδικού', 'woocommerce' ); break;
        case 'Billing & Shipping' : $translated_text = __( 'Διεύθυνση', 'woocommerce' ); break;
        case 'No order has been made yet.' : $translated_text = __( 'Δεν υπάρχουν καινούργιες παραγγελίες.', 'woocommerce' );  break;
        case 'Browse products' : $translated_text = __( 'ΠΡΟΙΟΝΤΑ', 'woocommerce' );  break;
        case 'Ship to a different address?' : $translated_text = __( 'Αλλαγή διεύθυνση αποστολής', 'woocommerce' );  break;



        //Address
        case 'Billing Address' : $translated_text = __( 'Διεύθυνση λογαριασμού', 'woocommerce' ); break;
        case 'Shipping Address' : $translated_text = __( 'Διεύθυνση αποστολής', 'woocommerce' ); break;
    }

    return $translated_text;
}
add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );
