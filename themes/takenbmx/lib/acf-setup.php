<?php

/*-------------------------------------------------------------------------------------------------------
	 Theme options group
--------------------------------------------------------------------------------------------------------*/
if( function_exists('acf_add_options_page') )
{

	acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title' 	=> 'Theme Options',
		'menu_slug' 	=> 'theme-options',
		'capability' 	=> 'edit_posts',
		'parent_slug' 	=> '',
		'position'		=> false,
		'icon_url'		=> false,
		'redirect' 	=> false
	));

}


 /*==============================================================
     2. Hide ACF Admin on Staging/Production
 ================================================================*/
function awesome_acf_hide_acf_admin() {

    // get the current site url
    $site_url = get_bloginfo( 'url' );

    // an array of protected site urls
    $protected_urls = array(
        'https://www.takenbmx.gr', //staging
        'http://www.takenbmx.gr', //staging
    );

    // check if the current site url is in the protected urls array
    if ( in_array( $site_url, $protected_urls ) ) {

        // hide the acf menu item
        return false;

    } else {

        // show the acf menu item
        return true;

    }

}

add_filter('acf/settings/show_admin', 'awesome_acf_hide_acf_admin');


/*-----------------------------------------------------------------------------------
    ACF: Register Google Maps API KEY for acf
-----------------------------------------------------------------------------------*/
add_action('acf/init', 'my_acf_init');

function my_acf_init()
{
	acf_update_setting('google_api_key', 'AIzaSyBi-D-jomrcvkNGCaJtYCcQahXJ8Ptj-oU');
}

function register_scripts()
{
  wp_enqueue_script( 'googlemap-api' , 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAM_yb1wCAnB6kenjT-gR9f7NkFMROFJcw', array('jquery'), null, true );
}

add_action( 'wp_enqueue_scripts', 'register_scripts' );
