<?php

/*-------------------------------------------------------------------------------------
     My Custom Post Type: https://codex.wordpress.org/Post_Types
-------------------------------------------------------------------------------------*/
add_action( 'init', 'crew_custom_post_type');

function crew_custom_post_type()
{
    //Vars
	  $singular = 'Crew';
    $singular_lowercase = 'crew';
	  $plural = 'Crew';
	  $slug = 'taken-crew';
	  $post_type = 'crew';
	  $supports = array('title', 'editor', 'page-attributes', 'revisions');

    //Labels
	  $labels = array(
		'name' => _x( $plural, 'post type general name'),
		'singular_name' => _x( $singular, 'post type singular name'),
		'add_new' => _x('Add New', $singular_lowercase ),
		'add_new_item' => __('Add New '. $singular_lowercase),
		'edit_item' => __('Edit '. $singular_lowercase ),
		'new_item' => __('New '. $singular_lowercase ),
		'view_item' => __('View '. $singular_lowercase),
		'search_items' => __('Search '. $plural),
		'not_found' =>  __('No '. $singular .' found'),
		'not_found_in_trash' => __('No '. $singular .' found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => $plural
	  );

    //Args
	  $args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'query_var' => true,
		'rewrite' => Array('slug'=> $slug ),
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
    'menu_icon' => 'dashicons-groups',
		'supports' => $supports
	  );

	  register_post_type( $post_type, $args );
}


add_action( 'init', 'register_crew_taxonomy' );

function register_crew_taxonomy() 
{
	register_taxonomy(
		'crew_category',
		'crew',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'crew-category' ),
			'hierarchical' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => false
		)
	);
}
