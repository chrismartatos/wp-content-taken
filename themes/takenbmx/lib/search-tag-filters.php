<?php


/*-------------------------------------------------------------------------------------
       FN: Products filtering
-------------------------------------------------------------------------------------*/

function search_tag_filters_init($get_tag_params,$price_from,$price_to,$keywords,$term)
{
  //Filters init
  if(!empty($get_tag_params) || !empty($price_from) || !empty($price_to) || !empty($keywords)):

    global $term;

    //defaults vars - resets
    $price_filters = '';
    $build_tags_array = [];
    $search_query = '';
    $array = [];
    $count_array = [];

    /* WP QUERY ARGS - DEFAULTS */
    $post_type = 'product';
    $posts_per_page = '-1';
    $order_by = 'title';
    $order = 'ASC';

    //Build query args for tags
    $array = $get_tag_params;
    $count_array = count($array);

    if($count_array >= 1)
    {
      $build_tags_array = [];

      //Build tag filter array
      for($i = 0; $i < $count_array; $i++)
      {
        if($count_array == "")
        {
          /* Empty query */
          //silence
        }
        elseif($count_array == 1)
        {
          /* Filter for only one val */
          $build_tags_array[$i] = array( 'relation' => 'AND', array( 'taxonomy' => 'product_tag', 'field' => 'slug', 'terms' => array($array[$i])));
        }
        elseif($count_array > 1)
        {
          /* Filter init with multiple filters */
          $build_tags_array[$i] = array( 'taxonomy' => 'product_tag', 'field' => 'slug', 'terms' => array($array[$i]));
        }
      }//Close for loop
    }


    /********** SEARCH QUERY **********/
    if(!empty($keywords))
    {
      $search_query = $keywords;
    }

    /********** BUILD PRICES array **********/
    if(!empty($price_from) || !empty($price_to))
    {
      $prices_filters = array(
        'key' => '_regular_price',
        'value' => array($price_from,$price_to),
        'type' => 'numeric',
        'compare' => 'BETWEEN',
        'operator' => 'IN'
      );
    } else {
      $prices_filters = array();
    }

    /********** ARGS QUERY **********/
    $filters_aray = array(
      'post_type' => $post_type,
      'posts_per_page' => $posts_per_page,
      'ignore_sticky_posts'	=> 1,
      'post_status' => 'publish',
      'orderby' => $order_by,
      'order' => $order,
      'meta_query' => array($prices_filters),
      'tax_query' => array(
        //'relation' => 'AND',
    		array(
    			'taxonomy' => 'product_cat',
    			'field'    => 'slug',
    			'terms'    => $term,
    		),
        $build_tags_array
      ),
      's' => $search_query,
    );


    //WP query
    $tag_search_query = new WP_Query( $filters_aray );
    $relative_url = get_home_url();

    //Show Products
    if ( $tag_search_query->have_posts() )
    {
      $total_posts = $tag_search_query->found_posts;

      $result = 'result';

      if($total_posts > 1)
      {
        $result = 'results';
      }

      echo '<nav class="params-menu clear" data-term="">';
      echo '<span class="found-results" data-total="'.$total_posts.'" data-result="'.$result.'">'.__('Αναζήτηση: ','takenbmx').' </span>';
        get_template_part('templates/search-filters/search-filters-results-info');
      echo '<span class="clear-all"><a href="'.get_term_link($term,'product_cat').'">'.__('Show all','takenbmx').' <i class="fa fa-times" aria-hidden="true"></i></a></span>';
      echo '</nav>';

      echo '<ul class="products">';
    	while ( $tag_search_query->have_posts() )
      {
    		$tag_search_query->the_post();

    		wc_get_template_part( 'content', 'product' );
    	}
      echo '</ul>';

      wp_reset_postdata();
    }
    else
    {
      wc_get_template( 'loop/no-products-found.php' );
    }

  else:
    //Default products functionality if all the params are empty
    get_template_part('templates/woocommerce-elements/subcategory-products');

  endif;
}
