<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->

    <?php
    //Home top slideshow
      if(is_front_page()):
        //if(!wp_is_mobile()){ get_template_part('templates/intro'); }

        get_template_part('templates/news/featured-news-slideshow');
      endif;

      if(is_product())
      {
        //Fix bug for single product: Move header.php and footer outside templates
      } else {
        get_template_part('header');
      }
    ?>

    <?php
	    $padding = (is_page( array( 'crew', 'takenboys', 'takenkids' )))? ' style="padding-top:0;"' : '';
    ?>
    <div class="page-wrapper" role="document">
      <div class="main-content"<?php echo $padding; ?>>
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!--END .main -->
      </div><!--END .main-content -->
    </div><!--END .main-wrapper -->
    <?php
      if(is_product())
      {
        //Fix bug for single product: Move header.php and footer outside templates
      } else {
        get_template_part('footer');
      }
      wp_footer();
    ?>
  </body>
</html>
