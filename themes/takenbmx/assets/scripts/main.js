/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  var Sage = {
    // All pages
    'common': {
      init: function()
      {

        /****  INTRO
        --------------------------------------------------------------------*/
        var $body = $('body');
        var $intro = $("#intro");
        var $header = $("#main-header");
        var $products_link = $header.find(".menu-products");
        var $mini_cart = $('#header-cart-btn, #close-cart-btn');
        var $window = $(window);

        if($products_link.hasClass("active"))
        {
          $header.find(".menu-news").removeClass("active");
        }

        /****  Lazyload
        --------------------------------------------------------------------*/
      	$body.find(".lazyload-init:not(.lazy-loaded)").lazyloadINIT();

        /****  Header functions
        --------------------------------------------------------------------*/
        $header.scrollDownHeader();
        $header.headerFunctions();

        $("#main-nav").dropDownMenu();


        /*******  Cart toggle
        ========================================================*/
        $mini_cart.TAKEN_MiniCartToggle(['#mini-cart']);

        /*******  Added to cart
        ========================================================*/
        $('.woocommerce-message').TAKEN_AddToCartNotification();

        $( document.body ).on( 'added_to_cart', function()
        {
          $(window).MiniCart_AjaxRequest();

          setTimeout(function()
          {
            $('#mini-cart').addClass('menu-open');

            //Css animation
            $('body').addClass('product-added');
          },500);
        });

        /*******  Woocommerce: Ajax header-cart count
        ========================================================*/
        $window.WOO_miniCartCount();
        $('#ajax-mini-cart-data').MiniCart_RemoveItem();

        /****  Load more
        --------------------------------------------------------------------*/
        $("#ajax-pagina").ajaxLoadMore();

        /****  Products
        --------------------------------------------------------------------*/
        $("#products-cat-nav").productsNav();

        /****  Search filters
        --------------------------------------------------------------------*/
        $("#search-filters-menu").searchFilters();

        /****  Toggle categories for blog
        --------------------------------------------------------------------*/
        $("#toggle-mobile-menu").toggleCategories();

        /****  Toggle categories for blog
        --------------------------------------------------------------------*/
        $("#companies-slideshow").companiesSlides();

        /****  Crew
        --------------------------------------------------------------------*/
        $("#taken-crew .gallery").crewGallery();

      },
      finalize: function()
      {

        /****  Sticky menu
        --------------------------------------------------------------------*/
        $(".wrap-products-cat-nav").stickyMenu();
        $("body").touchBottomFN();

        /****  swipebox
        --------------------------------------------------------------------*/
        var $target_swipebox = $(".swipebox, .gallery-item a");

        if($target_swipebox.length)
      	{
        	$target_swipebox.swipebox({
        		hideBarsDelay: 20000
        	});
        }

        /****  Contact scroll
        --------------------------------------------------------------------*/
        $('#main-footer .address a, .menu-contact > a').contactScroll();


      }
    },
    // Home page
    'home': {
      init: function()
      {
        var $height = $("#main-slideshow").height(),
            $header = $("#main-header"),
            $body = $("body"),
            $window = $(window),
            $pos = $window.scrollTop();

        /****  Home Scrolling header
        --------------------------------------------------------------------*/
        $window.scroll(function()
        {
           $pos = $window.scrollTop();

           if($pos>=$height){ $header.addClass("fixed-header"); } else { $header.removeClass("fixed-header"); }
        });


        /****  Slideshow
        --------------------------------------------------------------------*/
        $("#main-slideshow").mainSlideshow();


        /****  Scroll logo
        --------------------------------------------------------------------*/
        $(".home").find("#main-header .centered-logo").click(function(e)
        {
            e.preventDefault();

            $("body,html").animate({
              scrollTop: $(".main-content").offset().top
            },400);

            return false;
        });

        /****  Intro
        --------------------------------------------------------------------*/
        $("#intro .enter-site").click(function(e)
        {
        	e.preventDefault();

        	$body.addClass("remove-intro");

        	return false;
        });
      },
      finalize: function() {
        $('.deactivate, .deactivate-link > a').click(function(e){ e.preventDefault(); });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);





/*-----------------------------------------------------------------------------------------
  FN: Mobile menu animation trigger
-----------------------------------------------------------------------------------------*/
$.fn.TAKEN_MiniCartToggle = function(arrayOfElements)
{

  var $target   = $(this);
  var $arr      = arrayOfElements;

  if($target.length)
  {
    //Toggle mini cart
    $target.off().on('click', function(e)
    {
      e.preventDefault();

      $target.toggleClass('menu-open');

      for (var i = 0, len = $arr.length; i < len; i++) {
        $($arr[i]).toggleClass('menu-open');
      }
    });

    //Close cart when click outside
    $('#page').on({
      click: function()
      {
        if($('#mini-cart').hasClass('menu-open'))
        {
          $("#close-cart-btn").click();
        }
      }
    },":not(#mini-cart)");
  }
};



/*==============================================================================
    Woocommerce: MiniCart: Update cart html
==============================================================================*/
$.fn.MiniCart_AjaxRequest = function(cart_notification)
{
  var $target = $(this);

  if($target.length)
  {
     var $mini_cart = $("#mini-cart");
     var _notification_msg = cart_notification || null;
     var data = {
       'action': 'mode_theme_update_mini_cart'
     };

     //HTTP REFERER
     var _wp_http_referer = $(".cart").find('input[name="_wp_http_referer"]').val() || "/";

     $mini_cart.removeClass("updated");

     $.post(woocommerce_params.ajax_url,data,function(response)
     {
       var html_cart = $(response).find("#ajax-mini-cart-data").html();
       var _cart_count = $(response).find("#ajax-mini-cart-data").attr('data-count');
       var $get_mini_cart = $("#ajax-mini-cart-data");

       //Append data
       $get_mini_cart.html(html_cart);

       //Callback: for header counter
       $( document.body ).trigger( 'takenbmx_cart_update' );

       //Callback: for cart toggle
       $('#header-cart-btn, #close-cart-btn').TAKEN_MiniCartToggle(['#mini-cart']);

       //Loader
       $("#cart-loader").removeClass("active");

       $mini_cart.addClass("updated");

       //If notification exisit
       if(_notification_msg)
       {
         var $get_notification = $("#cart-notifications");

         $get_notification.text(_notification_msg);

         //slide Down
         setTimeout(function() { $get_notification.slideDown(); },400);

         //Slide Up
         setTimeout(function() { $get_notification.slideUp(); },2400);
       }

       //Callback
       $('#ajax-mini-cart-data').MiniCart_RemoveItem();
     });
  }
  else { alert("We couldn't update the card."); }
};



/*==============================================================================
    Woocommerce: CALLBACK: Remove Item
==============================================================================*/
$.fn.MiniCart_RemoveItem = function()
{
  var $target = $(this);

  if($target.find('.mini_cart_item').length)
  {
    $target.off().on("click", ".remove-item", {}, function(e)
    {
      e.preventDefault();

      //Loader
      $("#cart-loader").addClass("active");

      var cart_notification = jQuery("#cart-notifications").attr("data-remove") || null;

      var WOO_remove_cart_api = woocommerce_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'remove_from_cart' );

      var remove_data = {
        'cart_item_key' : $(this).attr("data-cart_item_key")
       };

       //AJAX call
       $.post(WOO_remove_cart_api,remove_data,function(response)
       {
         if ( ! response )
         {
           return;
         }

         var _notification_msg = $("#cart-notifications").attr('data-remove') || null;
         var $get_html = response.fragments["div.widget_shopping_cart_content"];
         var $cart_contents = ($get_html)? $($get_html).find("#mini-cart").html() : '';
         var $mini_cart = $('#mini-cart');

         //Append data
         $mini_cart.html($cart_contents);

         //Callback: for header counter
         $( document.body ).trigger( 'takenbmx_cart_update' );

         //Callback: for cart toggle
         $('#header-cart-btn, #close-cart-btn').TAKEN_MiniCartToggle(['#mini-cart']);

         //Callback: Remove item link
         $('#ajax-mini-cart-data').MiniCart_RemoveItem();

         //Loader
         $("#cart-loader").removeClass("active");

         //Callback: Notifications
         if(_notification_msg)
         {
           var $get_notification = $("#cart-notifications");

           $get_notification.text(_notification_msg);

           //slide Down
           setTimeout(function() { $get_notification.slideDown(); },400);

           //Slide Up
           setTimeout(function() { $get_notification.slideUp(); },2400);
         }
       });

      return false;
    });
  }
};


/*----------------------------------------------------------------------------------
                  Add to mini cart message and scroll - Sinlge product
-----------------------------------------------------------------------------------*/
$.fn.TAKEN_AddToCartNotification = function()
{
  var $target = $(this);

  if($target.length)
  {
    //open cart based on message
    var add_to_cart_msg = $target;
    var $mini_cart = $('#mini-cart');

    if(add_to_cart_msg.length)
    {
      if(add_to_cart_msg.text().indexOf('added') !== -1)
      {
        //Cart animations
        $(window).load(function()
        {

        });

        $mini_cart.addClass('menu-open');

        //Css animation
        $('body').addClass('product-added');
      }
    }
  }
};


/*----------------------------------------------------------------------------------
                    FN: cart count
-----------------------------------------------------------------------------------*/
$.fn.WOO_miniCartCount = function()
{
  $( document.body ).on( 'takenbmx_cart_update', function()
  {
    console.log('EVENT: takenbmx_cart_update');

    var data = {
      'action': 'cart_count_retriever'
    };

    jQuery.post(wc_cart_fragments_params.ajax_url, data, function(response)
    {
      var _counter = response;

      $('.ajax-cart-counter').html(response);
    });
  });
};




/*----------------------------------------------------------------------------------
                      FN: STICKY MENU
-----------------------------------------------------------------------------------*/
$.fn.stickyMenu = function()
{
  var $sticky = $(this);
  var $window = $(window);
  var $stickyrStopper = $('#main-footer');

  if (!!$sticky.offset())
  {
    if($window.width() > 999)
    {
      var generalSidebarHeight = $sticky.innerHeight();
      var stickyTop = $sticky.offset().top;
      var stickOffset = 20;
      var stickyStopperPosition = $stickyrStopper.offset().top;
      var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
      var diff = stopPoint + stickOffset;

      $window.scroll(function()
      {
        var windowTop = $window.scrollTop(); // returns number

        if (stopPoint < windowTop)
        {
            $sticky.css({ position: 'absolute', top: diff });
        } else if (stickyTop < windowTop+stickOffset) {
            $sticky.css({ position: 'fixed', top: stickOffset });
        } else {
            $sticky.css({position: 'absolute', top: 'initial'});
        }
      });
    }
  }
};


/*----------------------------------------------------------------------------------
                      FN: Drop Down Menu
-----------------------------------------------------------------------------------*/
$.fn.dropDownMenu = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.find("ul > li.menu-item-has-children").hover(function()
    {
        $(this).addClass("hover-active");
      },function(){
        $(this).removeClass("hover-active");
    });
  }
};

/*----------------------------------------------------------------------------------
                      FN: Categories
-----------------------------------------------------------------------------------*/
$.fn.productsNav = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $children = $target.find(".menu-item-has-children");
    var $parent = "";

    $children.find(" > a").click(function(e)
    {
      e.preventDefault();

      var $sub_menu = $(this);
      var $parent = $(this).parents(".menu-item-has-children").eq(0);

      if($parent.hasClass("active"))
      {
        $parent.find(".sub-menu").slideUp();
        $parent.removeClass("active");
      } else {
        $parent.find(".sub-menu").slideDown();
        $parent.addClass("active");
      }

      return false;
    });
  }
};


/*----------------------------------------------------------------------------------
                     Callback: Lazy load for slideshow only
-----------------------------------------------------------------------------------*/
var lazyLoadSlide = function()
{
 var $carousel= $("#taken-crew  .gallery");

 if($carousel.length)
 {
   $carousel.each(function()
   {
     var $target_lazy_slides = $('.slick-active',this).find(".lazy-load-slide:not(.loaded-slide)");

     $target_lazy_slides.addClass("loaded-slide").lazyload({
       effect: "fadeIn"
     });
   });
 }
};


/*----------------------------------------------------------------------------------
                      Callback: main slideshow
-----------------------------------------------------------------------------------*/
$.fn.crewGallery = function()
{
    var $slider = $(this);

		if($slider.length)
		{
      $slider.each(function()
      {
        $(this).slick({
         autoplay: false,
         slide: ".slide-item",
         arrows: false,
         dots: true,
         infinite:false,
         slidesToShow: 1,
         slidesToScroll:1,
         customPaging : function(slider, i)
         {
             return '<a class="dot dot-'+i+'"></a>';
         }
       });

       //Callback for slideshow only
       lazyLoadSlide();

       //Event: after slide change
       $(this).on('afterChange', function(event, slick, currentSlide, nextSlide)
       {
         //Callback for slideshow only
         lazyLoadSlide();
       });

      });
    }

    /****  CREW Scroll
    --------------------------------------------------------------------*/
    if($(".scroll-to-crew").length)
    {
      $(".scroll-to-crew").click(function(e)
      {
        e.preventDefault();

        var $id = $(this).attr('href'),
            offset = 0,
            speed = 800,
            callback;

        if($(this).data('offset'))
        {
          offset = parseInt($(this).data('offset'));
        }

        var top = $($id).offset().top - offset;

        // animation
        if(!$('html:animated, body:animated').length)
        {
          $('html,body').animate({scrollTop:top},speed);
        }

        $(this).addClass('scroll-to-on-crew');

        return false;
      });
    }
};


/*----------------------------------------------------------------------------------
                      Callback: main slideshow
-----------------------------------------------------------------------------------*/
$.fn.companiesSlides = function()
{
    var $slider = $(this);

		if($slider.length)
		{
      $slider.find(".wrap-slides").slick({
       autoplay: true,
       slide: ".slide-item",
       autoplaySpeed: 1200,
       arrows: false,
       dots: false,
       infinite:true,
       slidesToShow: 8,
       slidesToScroll:1,
       lazyLoad: 'progressive',
       responsive: [
       {
         breakpoint: 999,
         settings: {slidesToShow:6}
       },
       {
         breakpoint: 780,
         settings: {slidesToShow:3}
       }
      ]
     });
    }
};



/*----------------------------------------------------------------------------------
                      FN: Lazyload
-----------------------------------------------------------------------------------*/
$.fn.lazyloadINIT = function()
{
  var $this = $(this);

  if($this.length)
  {
    $this.each(function()
    {
      $(this).lazyload({
        effect: "fadeIn"
      });
    }).promise().done(function()
    {
      $(this).addClass("lazy-loaded");
    });
  }
};

/*----------------------------------------------------------------------------------
                      FN: scroll down
-----------------------------------------------------------------------------------*/
$.fn.scrollDownHeader = function()
{
	var didScroll;
	var lastScrollTop = 0;
	var delta = 1000;
	var $header = $("body");
	var navbarHeight = $("#main-header").outerHeight();

	$(window).scroll(function(event)
  {
	    didScroll = true;
	});

	setInterval(function()
	{
	    if(didScroll)
	    {
	        var st = $(this).scrollTop();

		    // Make sure they scroll more than delta
		    if(Math.abs(lastScrollTop - st) <= delta)
        {
			    if (st > lastScrollTop && st > navbarHeight)
			    {
			        // Scroll Down
			         $header.addClass("scrolling-down");
			    } else {
			        // Scroll Up
			        if(st + $(window).height() < $(document).height())
			        {
			          $header.removeClass("scrolling-down");
			        }
			    }

		      lastScrollTop = st;

	        didScroll = false;

        }
	    }
	}, 250);
};

/*----------------------------------------------------------------------------------
                      FN: Header
-----------------------------------------------------------------------------------*/
$.fn.headerFunctions = function()
{
  var header = $(this),
      $window = $(window),
      $pos = $window.scrollTop(),
      $gutter = header.height(),
      $hamburger = $("#hamburger");

  //Search
  header.find('#header-search').click(function(e)
  {
    e.preventDefault();

    if($(this).hasClass("active"))
    {
      header.removeClass('search-active');
    } else {
      header.addClass('search-active');
    }
  });

  //Close search
  header.find("#hide-search").click(function(e)
  {
    e.preventDefault();

    header.removeClass('search-active');
  });

  //Hamburger
  $hamburger.click(function(e)
	{
		e.preventDefault();

		var $this = $(this),
			$menu = $("#mobile-fixed-nav"),
			$body = $("body");


		$this.toggleClass("active");
		$body.toggleClass("mobile-menu-open");
		$menu.toggleClass("opened");

		return false;
	});

  $("#close-mobile-menu").click(function(e)
  {
    $hamburger.click();

    return false;
  });

  $window.scroll(function()
  {
   $pos = $window.scrollTop();

   if($pos>=$gutter){ header.addClass("scroll-init"); } else { header.removeClass("scroll-init"); }
  });

};



/*----------------------------------------------------------------------------------
                      FN: Contact
-----------------------------------------------------------------------------------*/
$.fn.contactScroll = function()
{
  var target = $(this);

  if(target.length)
  {
    //Contact
    target.click(function(e)
    {
      e.preventDefault();

      var $footer = $("#main-footer"),
          $map = $('#map'),
          $scroll_to = $footer.offset().top,
          height = $footer.height(),
          $offset = $scroll_to-height;


        $("html, body").animate({
          scrollTop: $offset
        },800).promise().done(function()
        {
          if($("body").hasClass("mobile-menu-open"))
          {
            $("#hamburger").click();
          }

          if(!$(this).hasClass("active"))
          {
            $map.animate({
              height: "320px"
            },600).promise().done(function()
            {
              $("html, body").animate({ scrollTop: $scroll_to },800);
              $map.googleMap();
            });
          }
        });

        $(this).addClass("active");

      return false;
    });
  }
};



/*----------------------------------------------------------------------------------
                      Callback: main slideshow
-----------------------------------------------------------------------------------*/
$.fn.mainSlideshow = function()
{
    var $slider = $(this);

		if($slider.length)
		{
      var $slide_item = $slider.find('.slide-item');

      $slide_item.bgLoaded({
        afterLoaded : function()
        {
           $slider.addClass('images-loaded');
           $("body").addClass('slideshow-loaded');
     		}
      });

      $slider.slick({
       autoplay: false,
       slide: ".slide-item",
       prevArrow: ".prev-arrow",
       nextArrow: ".next-arrow",
       autoplaySpeed:4000,
       arrows: true,
       dots: true,
       infinite:true,
       slidesToShow: 1,
       slidesToScroll:1,
       pauseOnDotsHover: true,
       customPaging : function(slider, i)
       {
           return '<a class="dot dot-'+i+'"></a>';
       },
        appendDots: "#append-dots",
        responsive: [
        {
          breakpoint: 1100,
          settings: {slidesToShow:1}
        },
        {
          breakpoint: 780,
          settings: {slidesToShow:1}
        }
      ]
     });

    }
};


/*----------------------------------------------------------------------------------
                    FN: CSS Background images loaded
-----------------------------------------------------------------------------------*/
$.fn.bgLoaded = function(custom)
{
 	var self = this;

	// Default plugin settings
	var defaults = {
		afterLoaded : function(){
			this.addClass('bg-loaded');
		}
	};

		// Merge default and user settings
		var settings = $.extend({}, defaults, custom);

		// Loop through element
		self.each(function()
    {
			var $this = $(this),
				bgImgs = $this.css('background-image').split(', ');
			$this.data('loaded-count',0);

			$.each( bgImgs, function(key, value){
				var img = value.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
				$('<img/>').attr('src', img).load(function() {
					$(this).remove(); // prevent memory leaks
					$this.data('loaded-count',$this.data('loaded-count')+1);
					if ($this.data('loaded-count') >= bgImgs.length) {
						settings.afterLoaded.call($this);
					}
				});
			});
		});
};



/*==========================================================
    Declare jQuery functions - Table of content
------------------------------------------------------------
      - Toggle categories
      - Background images preloader for slideshow
      - SEARCH FILTERS
        -- search api call
        -- fitlering init
        -- events and params
        -- active class
===========================================================*/



/*----------------------------------------------------------------------------------
                      FN: Toggle categories
-----------------------------------------------------------------------------------*/
$.fn.toggleCategories = function()
{
  var $cat_menu_btn = $(this);

  if($cat_menu_btn.length)
  {
    var $cat_menu_wrap = $cat_menu_btn.find(".toggle-menu");
    var $menu = $("#toggle-mobile-menu");

    $cat_menu_btn.find(".toggle-title").click(function(e)
    {
      e.preventDefault();

      if($(this).hasClass("active"))
      {
        //Close
        $cat_menu_wrap.slideUp().promise().done(function(){
          $menu.toggleClass("active");
        });

        //Toggle class
        $(this).toggleClass("active");
      }
      else
      {
        //Open
        $cat_menu_wrap.slideDown().promise().done(function(){
          $menu.toggleClass("active");
        });

        //Toggle class
        $(this).toggleClass("active");
      }

      return false;
    });
  }
};

/*----------------------------------------------------------------------------------
                    FN: Render html
-----------------------------------------------------------------------------------*/
var renderHtml = function(html)
{
	var result = String(html).replace(/<\!DOCTYPE[^>]*>/i, '')
							 .replace(/<(html|head|body|title|script)([\s\>])/gi,'<div id="get-html-$1"$2')
							 .replace(/<\/(html|head|body|title|script)\>/gi,'</div>');

	return result;
};

/*----------------------------------------------------------------------------------
                    FN: Load More
-----------------------------------------------------------------------------------*/
$.fn.ajaxLoadMore = function()
{
 //Target pagination
 var $loadMore = $(this);

 if($loadMore.length)
 {
   //Target button
   var $nextBtn = $loadMore.find(".nav-previous a");
   var $paged = $loadMore.find(".nav-previous");
   var $cur = $paged.attr("data-current");
   var $max = $paged.attr("data-max");
   var $this = "";

   if($cur === $max)
   {
     //$nextBtn.hide();
   }

   $nextBtn.click(function( e )
   {
       e.preventDefault();

       //Defaults
       var $this = $(this),
           $page = $this.attr("href"),
           $wrapper = $(".paging-content"),
           $last_item = $wrapper.find(".paging-row:last");

         //Show spinner
         $this.toggleClass("active");

         $.ajax({
           url: $page,
           success: function( html, textStatus, jqXHR )
           {
             var $data	= $(renderHtml(html)),
                 $next_href = $data.find("#ajax-pagina").html(),
                 $posts = $data.find(".paging-content .paging-row").html();

             //Get href for next
             $loadMore.html($next_href);

             //Append items
             $last_item.after('<div class="paging-row ajax-paging">'+$posts+'</div>');

             //hide pagination if there is no more pages or toggle spinner
             if($data.find(".nav-previous > a").length)
             {
               $this.toggleClass("active");
             }
             else
             {
               $loadMore.hide().addClass("no-more-paging");

               $this.toggleClass("active");
             }

           },
           error: function( jqXHR, textStatus, errorThrown )
           {
             //Error
             alert( 'The following error occured: ' + textStatus +' Try to refresh the page or contact us. ' + errorThrown);

             $this.toggleClass("active");
           },
           complete: function( jqXHR, textStatus, errorThrown )
           {
             //callbacks
             var $height = $("#main-header").height()+20;
             var $row = $wrapper.find(".paging-row:last");

             $row.find(".lazyload-init").lazyloadINIT();

             $("body, html").animate({
               scrollTop: $wrapper.find(".paging-row:last").offset().top-$height
             },600);

             $("#ajax-pagina").ajaxLoadMore();
           }
         });

         return false;

   });//End .click()
 }
};



/*----------------------------------------------------------------------------------
              FN: Window location.search api for searching params
-----------------------------------------------------------------------------------*/
$.fn.search_products_init = function(search_data)
{
  if(search_data !== "")
  {
    location.search = search_data;
  }
  else
  {
    //Reset - go to default
    window.location = $("#search-filters-menu").attr("data-href");
  }
};

/*----------------------------------------------------------------------------------
                    FN: Search filters init
-----------------------------------------------------------------------------------*/
$.fn.searchFilters = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $menu_tag = $target.find('.select-filter'),
        $menu_price = $target.find('.select-price'),
        $search_form = $target.find("#search-keywords"),
        $window = $(window),
        _search_val = "",
        _search_param = "",
        _count_filter_items = $target.find("fieldset.menu-item").length || 1,
        $search_check = window.location.href;

    //Disable form
    $target.find("form").prop("disabled", true);

    $('body').addClass('filter-items-'+_count_filter_items);

    //Get active select filters
    if ($search_check.indexOf("?") !== -1)
    {
      $window.activeFilters();
    }

    //Filtering button event
    $target.find(".menu-item select").change(function(e)
    {
      e.preventDefault();

      _search_val = $search_form.val() || false;

      if(!_search_val)
      {
        $menu_tag.tagFiltersEvents($menu_tag,$menu_price);
      }
      else
      {
        _search_val = $search_form.val();

        if(_search_val.length > 0 && _search_val !== "")
        {
          _search_param = "keywords="+_search_val;

          $window.search_products_init(_search_param);
        } else {
          $menu_tag.tagFiltersEvents($menu_tag,$menu_price);
        }
      }
    });

    //Event for Enter key - for input search
    if($search_form.length)
    {
      //Search Event on enter
      $search_form.bind('keypress keydown keyup', function(e)
      {
        _search_val = $search_form.val();

        if(_search_val.length > 0 && _search_val !== "")
        {
          _search_param = "keywords="+_search_val;

          //One enter
          if(e.keyCode === 13)
          {
            e.preventDefault();

            $window.search_products_init(_search_param);
          }
        }
      });
    }//End if search form

  }
};


/*----------------------------------------------------------------------------------
                    FN: tag menus events - prices - tags
-----------------------------------------------------------------------------------*/
$.fn.tagFiltersEvents = function($menu_tag,$menu_price)
{
  if($menu_tag.length || $menu_price.length)
  {
    //Defaults
    var params_uri = "",
        _tag_params = {},
        $window = $(window);

    if($menu_tag.length)
    {
      $menu_tag.each(function(i)
      {
        var $val = $("option:selected", this).attr("val");

        if($val!=="")
        {
          var param_name = $("option:selected", this).attr("data-param"),
              param_val = $("option:selected", this).attr("data-val");

          //Build object
          _tag_params[param_name] = param_val;
        }

        return _tag_params;
      });

      //console.log($.param(_tag_params));

      if(!$menu_price.length)
      {
        /* use only tag params
        -------------------------------------------------------*/
        params_uri = $.param(_tag_params);

        /* Search request */
        $window.search_products_init(params_uri);
      }
    }

    /*If menu price existe merge price params with tag params
    -------------------------------------------------------*/
    if($menu_price.length)
    {
      //defaults
      var _price_params_from = {},
          _price_params_to = {},
          _merge_prices = {},
          _merge_price_tags = {};

      $menu_price.each(function(i)
      {
        var $val = $("option:selected", this).attr("val");

        if($val!=="")
        {
          var param_from_name = $("option:selected", this).attr("data-from-param"),
              param_from_val = $("option:selected", this).attr("data-from-val"),
              param_to_name = $("option:selected", this).attr("data-to-param"),
              param_to_val = $("option:selected", this).attr("data-to-val");

               _price_params_from[param_from_name] = param_from_val;
               _price_params_to[param_to_name] = param_to_val;

               _merge_prices = $.extend({},_price_params_from,_price_params_to);
        }

        return _merge_prices;
      });

      //Merge two objects tags and prices
      _merge_price_tags = $.extend({},_tag_params,_merge_prices);

      params_uri = $.param(_merge_price_tags);

      /* Search request */
      $window.search_products_init(params_uri);

      //console.log("Prices: "+params_uri);

    }//end if menu price

  }//end if for $menu_tag or $menu_price
};


/*----------------------------------------------------------------------------------
              FN: show active select filters
-----------------------------------------------------------------------------------*/
$.fn.activeFilters = function()
{
  //defaults
  var _this = $(this);
  var _undefined = "undefined";
  var tag_filters = $("#search-filters-menu select");

  if(_this.length)
  {
    //Parse query
    query.parse({ query: window.location.search.substring(1) });

    //Get params
    var _brand     = query.get('brand'),
        _color     = query.get('color'),
        _size    = query.get('size'),
        _country   = query.get('country'),
        _priceFrom = query.get('priceFrom'),
        _priceTo   = query.get('priceTo');

    //Uncomment for dubugging
    //console.log("brand:"+_brand+" | color:"+_color+" | country: "+_country+" | style: "+_style+" | prices: from="+_priceFrom+" and to="+_priceTo);

    //Brand select
    if(_brand !== _undefined)
    {
      var _target_brand = tag_filters.find('#option-'+_brand);

      if(_target_brand.length)
      {
        _target_brand.prop('selected', true);
      }
    }

    //Color select
    if(_color !== _undefined)
    {
      var _target_color = tag_filters.find('#option-'+_color);

      if(_target_color.length)
      {
        _target_color.prop('selected', true);
      }
    }

    //Style select
    if(_size !== _undefined)
    {
      var _target_size = tag_filters.find('#option-'+_size);

      if(_target_size.length)
      {
        _target_size.prop('selected', true);
      }
    }

    //Country select
    if(_country !== _undefined)
    {
      var _target_country = tag_filters.find('#option-'+_country);

      if(_target_country.length)
      {
        _target_country.prop('selected', true);
      }
    }

    //Prices select
    if(_priceFrom !== _undefined && _priceTo !== _undefined)
    {
      var _target_prices = tag_filters.find('#option-'+_priceFrom+_priceTo);

      if(_target_prices.length)
      {
        _target_prices.prop('selected', true);
      }
    }
    //tag_filters.change();
  }
};


/*------------------------------------------------------------------------------------
    Declare FN: Google map - JS only for one MARKER - not multiple locations
-------------------------------------------------------------------------------------*/
$.fn.googleMap = function()
{
  //Get elements and content
  var $map = $(this);

  if($map.length)
  {
    //Defaults
    var $target = $map.find(".marker"),
        $icon_img = null,
        infowindow = null,
        html_content = null,
        snazzymaps_array = null,
        _zoom = 16;

    if($target.length)
    {
      //Check for snazzymaps - ACF field
      if (typeof snazzymaps !== 'undefined')
      {
          snazzymaps_array = snazzymaps;
      }

      var getLat = $target.attr("data-lat"),
          getLng = $target.attr("data-lng"),
          _address = $target.attr("data-address"),
          _icon = $target.attr("data-icon"),
          _content = $target.html(),
          _style = snazzymaps_array;

      if(!$target.hasClass("active"))
      {
        $target.addClass("active");

        if(_address.length)
        {
          // Map options
          var options = {
              center: new google.maps.LatLng(getLat,getLng),
              scrollwheel : false,
              zoom: _zoom,
              styles: _style
            },
            map = new google.maps.Map(document.getElementById('map'), options);

          var geocoderMarker = new google.maps.Geocoder();

          geocoderMarker.geocode( { 'address': _address}, function(results, status)
          {
            if (status === google.maps.GeocoderStatus.OK)
            {
              //Set marker
              var marker = new google.maps.Marker({
                position: results[0].geometry.location,
                map: map,
                title: _address,
                icon: _icon
              }),
              infowindow = new google.maps.InfoWindow({
                content: _content
              });

              //Info window listener
              if(_content.length)
              {
                marker.addListener('click', function()
                {
                  infowindow.open(map, marker);
                });
              }
            }
          });
        }
      } else { alert("No address found"); } //END if(_address.length)
    }//END if($target.length)
  }//END if($map.length)

}; /*END googleMap FN */


/*-------------------------------------------------------------------------------
    Touched Bottom
-------------------------------------------------------------------------------*/
$.fn.touchBottomFN = function()
{
  //Target body
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window), $document = $(document);

    $window.scroll(function()
		{
			var beforeBottom = 540,
          _height = $document.height()-beforeBottom,
          w_height = $window.height(),
          total = $window.scrollTop()+w_height;

       if(_height<total)
       {
         $target.addClass("touched-bottom");
       } else {
	       $target.removeClass("touched-bottom");
       }
	   });
  }
};

})(jQuery); // Fully reference jQuery after this point.
