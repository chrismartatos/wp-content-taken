<?php
  /** Companies slideshow **/
  get_template_part('templates/elements/companies');

  /** Youtube feed **/
  //get_template_part('templates/elements/youtube-feed');

  /** GOOGLE MAP **/
  get_template_part('templates/elements/googlemap');
?>

<footer id="main-footer">
  <div class="inner clearfix">

    <!--Logo-->
    <?php get_template_part('templates/logos/taken-logo'); ?>

    <!--Footer info-->
    <?php
      $address = get_field('address','option');
      $email = get_field('email','option');
      $phone = get_field('phone','option');
      $hours = get_field('hours','option');
    ?>
    <div class="store-info">
      <div class="address"><?php echo $address; ?></div>
      <?php if($email): ?>
      <span class="email"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></span>
      <?php endif; ?>
      <?php if($phone): ?>
      <span class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone; ?></span>
      <?php endif; ?>
    </div>

    <!--Hours-->
    <div class="hours">
      <?php echo $hours; ?>
    </div>

    <!--Social-->
    <?php get_template_part( 'templates/social-media-icons' ); ?>

  </div>

  <!--Copyrights-->
  <address id="copyrights"><?php the_field('copyrights','option'); ?></address>
</footer>
