<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/acf-setup.php', //ACF field groups options
  'lib/wp-galleries.php',
  'lib/content-lazy-swipebox.php',
  'lib/search-tag-filters.php',
  'lib/cpt-crew.php',
  'lib/woo.php',
];

foreach ($sage_includes as $file)
{
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


/*-------------------------------------------------------------------------------------
         Add SVG capabilities
-------------------------------------------------------------------------------------*/
function wpcontent_svg_mime_type( $mimes = array() )
{
  $mimes['svg']  = 'image/svg+xml';
  $mimes['svgz'] = 'image/svg+xml';

  return $mimes;
}
add_filter( 'upload_mimes', 'wpcontent_svg_mime_type' );


/*-------------------------------------------------------------------------------------
       Read More
-------------------------------------------------------------------------------------*/
function modify_read_more_link()
{
    return '<a class="more-link" href="' . get_permalink() . '">Read Post <i class="fa fa-chevron-right" aria-hidden="true"></i></a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );


/**
 * Disable post embeds for internal links but allow it on external sites
 */
add_filter( 'pre_oembed_result', function( $result, $url, $args )
{
    if( parse_url( home_url(), PHP_URL_HOST ) ===  parse_url( $url, PHP_URL_HOST ) )
        $result = false;
    return $result;

}, PHP_INT_MAX, 3 );


// Add to existing function.php file
// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'df_disable_comments_post_types_support');
// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');
// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');
// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'df_disable_comments_admin_bar');
