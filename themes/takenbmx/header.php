<?php
//WOOCOMMERCE MINI CART
get_template_part('woocommerce/cart/mini-cart');
?>

<div id="mobile-fixed-nav">

  <button id="close-mobile-menu">
    <i class="fa fa-times" aria-hidden="true"></i>
  </button>

  <nav class="nav-wrap">
    <?php
      wp_nav_menu([
        'theme_location' => 'mobile_nav',
      ]);
    ?>
	</nav>
</div>

<header id="main-header">
  <div class="inner clearfix">

    <!--Logo-->
    <?php get_template_part('templates/logos/taken-logo'); ?>


    <?php $logo_header = get_field('image','option'); ?>

    <a class="centered-logo" href="<?php echo get_home_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
      <img src="<?php echo $logo_header['url']; ?>" alt="<?php bloginfo( 'name' ); ?>">
    </a>



    <nav id="mobile-nav">
			<a id="hamburger" class="hamburger" href="#">
				<span class="line line-1"></span>
				<span class="line line-2"></span>
				<span class="line line-3"></span>
			</a>
		</nav>

    <!-- Primary -->
    <nav id="main-nav">
       <?php
       if (has_nav_menu('primary_navigation')) :
          wp_nav_menu( array(
              'menu'              => 'primary_navigation',
              'theme_location'    => 'primary_navigation',
              )
          );
        endif;
        ?>
    </nav>

    <div class="header-right-nav clearfix">
      <!--Social-->
      <?php get_template_part('templates/social-media-icons'); ?>

      <div class="account-menu">
        <ul>
          <li class="icon-account">
              <?php if ( is_user_logged_in() ) { ?>
                  <a class="icon logged-in-link" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"></a>
              <?php }
              else { ?>
                  <a class="icon logged-out-link" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"></a>
              <?php } ?>
          </li>
          <li class="icon-cart">
            <?php $activate_toggle = (is_cart() || is_checkout())? 'deactivated-header-cart-btn' : 'header-cart-btn'; ?>

            <a id="<?= $activate_toggle; ?>" href="<?php echo get_permalink( get_option('woocommerce_cart_page_id') ); ?>" class="icon">
              <span class="ajax-cart-counter count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
            </a>
          </li>
        </ul>
      </div><!-- END .account-menu -->

      <nav class="search-wrap desktop-only">
        <a id="header-search" class="btn-search global-search" href="#">
          <i class="fa fa-search" aria-hidden="true"></i>
        </a>
        <a id="hide-search" class="btn-search close-search" href="#">
          <i class="fa fa-times" aria-hidden="true"></i>
        </a>
      </nav>

      <!-- Cart -->
      <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"></a>

    </div><!--.header-right-nav-->



    <!--Form-->
    <?php get_template_part('templates/search-form'); ?>

  </div>
 </header>
