<?php
$image_url_medium = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium', true);
$feat_img_thumb = $image_url_medium[0];
?>

<?php if(!empty($feat_img_thumb)): ?>
<div class="post-img-header bg-cover" style="background-image:url(<?php echo $feat_img_thumb; ?>);"></div>
<?php endif; ?>

<?php if(empty($feat_img_thumb)): ?>
<div class="post-header">
  <div class="container">
    <div class="row">
      <div class="col-lg-1"></div>
      <header class="col-lg-10">
        <?php while (have_posts()) : the_post(); ?>
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php get_template_part('templates/entry-meta'); ?>
        <?php endwhile; ?>
      </header>
      <div class="col-lg-1"></div>
    </div>
  </div>
</div>
<?php endif; ?>

<div class="container">
  <div class="row">
    <div class="col-lg-1"></div>
    <?php while (have_posts()) : the_post(); ?>
      <article <?php post_class('col-lg-10'); ?>>
        <div class="wrap-entry clearfix">
          <?php if(!empty($feat_img_thumb)): ?>
          <header class="post-header-internal">
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php get_template_part('templates/entry-meta'); ?>
          </header>
          <?php endif; ?>

          <div class="entry-content">
            <?php get_template_part('templates/news/single-post-meta'); ?>

            <div class="post-content clearfix">
              <?php the_content(); ?>
            </div>
            <?php get_template_part('templates/news/single-post-meta-bottom'); ?>

            <?php get_template_part('templates/news/single-post-nav'); ?>
          </div>
        </div>

        <?php comments_template('/templates/comments.php'); ?>
      </article>
    <?php endwhile; ?>
    <div class="col-lg-1"></div>
  </div>
</div>
