
<?php if(is_product_category() === true || is_shop() === true ): ?>

<?php else: ?>
<?php endif; ?>
<div class="youtube-feed bg-cover">
  <div class="container-fluid">
    <header class="subscribe clearfix">
      <script src="https://apis.google.com/js/platform.js"></script>
      <script>
        var __ytRIL = null;
      </script>
      <div class="g-ytsubscribe" data-channelid="UC1MlLsrXEbGouWGdIRPVA8g" data-layout="full" data-count="hidden" data-onytevent="onYtEvent"></div>
    </header>
    <div class="clearfix">
      <?php
      $channel_id = 'UC1MlLsrXEbGouWGdIRPVA8g';
      $api_key = 'AIzaSyBi-D-jomrcvkNGCaJtYCcQahXJ8Ptj-oU';
      //$api_key_live_site = 'AIzaSyByOGVeH8sPbEHGH37KRE-bQxCBlkVnNBo';
      $show_only = (wp_is_mobile()) ? 2 : 4 ;

      $json_url = "https://www.googleapis.com/youtube/v3/search?key=".$api_key."&channelId=".$channel_id."&part=snippet,id&order=date&maxResults=".$show_only;

      //  Initiate curl
      $ch = curl_init();
      // Disable SSL verification
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      // Will return the response, if false it print the response
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      // Set the url
      curl_setopt($ch, CURLOPT_URL,$json_url);
      // Execute
      $result = curl_exec($ch);
      // Closing
      curl_close($ch);

      // Will dump a beauty json :3
      $listFromYouTube = json_decode($result, true);

      if(!empty($listFromYouTube['items']))

         foreach($listFromYouTube['items'] as $video):
            $id = $video['id']['videoId'];
            $title = $video['snippet']['title'];
            $desc = $video['snippet']['description'];
            $img = $video['snippet']['thumbnails']['medium']['url'];
            ?>
            <article class="slide-item col-xs-6 col-sm-4 col-md-3">
              <a class="video-wrap bg-cover swipebox lazyload-init" rel="youtube" href="https://www.youtube.com/watch?v=<?php echo $id; ?>" data-original="<?php echo $img ?>" target="_blank">
                <div class="hover">
                  <span class="play-icon"></span>
                  <div class="wrap-title <?php if(empty($desc)): echo "no-desc"; endif; ?>">
                    <h5><?php echo $title; ?></h5>
                  </div>

                  <?php if(!empty($desc)): ?>
                  <div class="details">
                    <div class="vcenter-outer">
                      <div class="vcenter-inner">
                        <p class="desc"><?php echo $desc; ?></p>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>

                </div>
              </a>
            </article>
            <?php
         endforeach;

       else

       echo '<span class="error">An error has occured.</span>';
      ?>
    </div>
  </div>
</div>
