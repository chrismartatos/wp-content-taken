
<?php if(is_shop() === true || is_product_category() === true): ?>

<?php $title = get_field('comp_title','option'); ?>

<div id="companies-slideshow">
  <section class="container-fluid">
  <?php if($title): ?>
  <header class="title-secondary">
    <h5 class="title"><?= $title; ?></h5>
  </header>
  <?php endif; ?>

  <div class="row">
    <div class="wrap-slides clearfix">
      <?php
        if( have_rows('add_logos','option') ):

          while ( have_rows('add_logos','option') ) : the_row();

          $logo = get_sub_field('logo');
          ?>
          <article class="col-xs-4 col-sm-3 col-md-2 slide-item bg-cover clearfix">
            <img src="<?php echo $logo['url']; ?>" alt="takenbmx shop"/>
          </article>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
  </section>
</div>
<?php endif; ?>
