<?php use Roots\Sage\Titles; ?>

<?php
  $image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', true);
  $feat_img = $image_url_full[0];
?>

<div class="page-header-container">
  <div class="page-main-header bg-cover"<?php if( !empty($feat_img) ){ echo ' style="background-image:url('.$feat_img.');"'; } ?>>
    <h1><?= Titles\title(); ?></h1>
  </div>
</div>
