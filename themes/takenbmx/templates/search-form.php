<div class="search-form">
   <form role="search" method="get" class="search-form" action="<?= get_home_url(); ?>/">
     <div class="search-area">
         <input type="search" class="search-field" placeholder="Enter Search" value="" name="s" title="">
         <button type="submit" class="search-submit">
           <i class="fa fa-search" aria-hidden="true"></i>
         </button>
       </div>
   </form>
</div>
