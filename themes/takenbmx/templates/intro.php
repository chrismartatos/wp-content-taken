<script type="text/javascript">
if(sessionStorage.getItem('intro')==="first_visit"){document.body.className += ' ' + 'remove-intro';}
if(!sessionStorage.getItem('intro')){document.body.className += ' ' + 'add-intro';sessionStorage.setItem('intro', 'first_visit');}
</script>
<?php
$intro = get_field('intro_video','option');
$intro_title = get_field('intro_title','option');
?>
<div id="intro">
  <div class="vcenter-outer">
    <div class="vcenter-inner">

      <?php if($intro): ?>
      <div class="video-wrapper">
        <div class="video-container">
          <?php echo $intro; ?>
        </div>
      </div>
      <?php endif; ?>
      <a class="enter-site taken-logo" href="#" title="<?php bloginfo('name'); ?>"></a>
      <a class="enter-site" href="#" title="<?php bloginfo('name'); ?>">
        <span class="title"><?= $intro_title; ?></span>
      </a>
    </div>
  </div>
</div>
