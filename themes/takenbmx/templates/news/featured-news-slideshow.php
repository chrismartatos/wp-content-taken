<?php
//Query
$news_args = array(
  'post_type' => 'post',
  'cat' => 20,
  'posts_per_page' => 5,
  'ignore_sticky_posts'	=> 1,
  'post_status' => 'publish',
  'orderby' => 'date',
);

$latest_news = new WP_Query( $news_args );
?>

<div id="main-slideshow" class="lazy-slideshow">
  <div class="css-loader"></div>
  <div id="controls">
    <div class="wrap-controls">
      <div class="wrap-arrows">
        <a class="fa fa-angle-left prev-arrow"></a>
      	<a class="fa fa-angle-right next-arrow"></a>
      </div>

      <div id="append-dots"></div>
    </div>
  </div>


  <?php

  if($latest_news->have_posts()):

		while ($latest_news->have_posts()) : $latest_news->the_post();

		$image_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'medium', true);
		$postid = get_the_ID();
	?>
		<div class="slide-item bg-cover" data-date="<?php the_time(get_option('date_format')); ?>"  style="background-image:url(<?php echo $image_url[0]; ?>);" data-original="<?php echo $image_url[0]; ?>">
			<div class="bg"></div>
			<a class="wrap" href="<?php the_permalink(); ?>" data-id="<?php echo $postid; ?>" title="<?php the_title(); ?>">
        <time class="date"><?php the_time(get_option('date_format')); ?></time>
        <h3 class="title"><?php the_title(); ?></h3>
				<div class="read-more" data-author="<?php the_author(); ?>"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> View Post</div>
			</a>
		</div>
	<?php
		endwhile;

    wp_reset_postdata();
  endif;

	?>
</div>
