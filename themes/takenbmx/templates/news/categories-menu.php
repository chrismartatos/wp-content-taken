<div class="wrap-cat-nav clearfix">
  <nav class="categories-nav">
    <?php
      wp_nav_menu([
        'theme_location' => 'news_menu',
      ]);
    ?>
  </nav>
</div>
