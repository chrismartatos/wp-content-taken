<div class="bottom-meta wrap-meta clearfix">
	<nav class="left">
		<?php get_template_part('templates/share-buttons'); ?>
	</nav>
	<?php
		$posttags = get_the_tags();

		if ($posttags)
	  {
			echo '<nav class="cat-nav tags right"><span class="title"></span>';
		  foreach($posttags as $tag)
		  {
		    echo '<a class="cat" href="'.get_home_url().'/tag/'.$tag->name.'">#'.$tag->name.'</a>';
		  }
		  	echo "</nav>";
		}
	?>
</div>
