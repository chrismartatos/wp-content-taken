<footer class="prev-next clearfix">
  <?php
    $next_post = get_next_post();
    $prev_post = get_previous_post();
  ?>
  <?php
  if (!empty( $next_post )):
    $feat_next = wp_get_attachment_image_src(get_post_thumbnail_id($next_post->ID), 'medium', true);
  ?>

    <a href="<?php echo get_permalink( $next_post->ID ); ?>" class="prev bg-cover nav-img" style="background-image:url(<?php echo $feat_next[0]; ?>)" title="<?php echo $next_post->post_title; ?>">
      <span class="arr left"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous</span>
      <div class="post-title"><?php echo $next_post->post_title; ?></div>
    </a>
  <?php endif; ?>

  <?php
  if (!empty( $prev_post )):
    $feat_prev = wp_get_attachment_image_src(get_post_thumbnail_id($prev_post->ID), 'medium', true);
  ?>
    <a href="<?php echo get_permalink( $prev_post->ID ); ?>" class="nav-img next bg-cover" style="background-image:url(<?php echo $feat_prev[0]; ?>)" title="<?php echo $prev_post->post_title; ?>">
      <span class="arr right">Next <i class="fa fa-arrow-right" aria-hidden="true"></i></span>
      <div class="post-title"><?php echo $prev_post->post_title; ?></div>
    </a>
  <?php endif; ?>

</footer>
