<div class="wrap-meta clearfix">
  <nav class="post-back left">
    <a href="javascript: history.go(-1);" onclick="window.history.back();"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Back</a>
  </nav>

  <nav class="category right cat-nav desktop-only">
  <span class="title"><i class="fa fa-tag" aria-hidden="true"></i> </span>
  <?php
  foreach(get_the_category() as $category)
  {
      echo '<a class="cat" href="'.get_category_link($category->cat_ID).'">'.$category->cat_name.'</a>';
  }
  ?>
  </nav>
</div>
