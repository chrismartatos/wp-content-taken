<?php
use Roots\Sage\Titles;

global $wp_query;
?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<div class="container">
  <div class="paging-content">
    <header class="title-secondary news-header">
      <h1 class="title"><?= Titles\title(); ?></h1>
    </header>
    <?php get_template_part('templates/news/categories-menu'); ?>
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-12">
        <div class="paging-row clearfix">
          <?php while (have_posts()) : the_post(); ?>
            <?php get_template_part('templates/news/entry-post'); ?>
          <?php endwhile; ?>
        </div>

        <?php if(isset($wp_query->max_num_pages)): ?>
        <nav id="ajax-pagina" class="ajax-pagination" data-current="1" data-max="<?php echo $wp_query->max_num_pages; ?>">
         <div class="nav-previous">
           <?php
           echo get_next_posts_link( 'Load More <span class="css-loader"></span>', $wp_query->max_num_pages );
           ?>
         </div>
        </nav>
        <?php endif; ?>
      </div>
      <aside id="sidebar" class="col-lg-4 col-md-4 col-sm-12">
        <?php get_template_part('templates/news/sidebar'); ?>
      </aside>
    </div>

  </div>
</div>
