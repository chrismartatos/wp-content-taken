<?php
$image_url_medium = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium', false);
$feat_img_thumb = $image_url_medium[0];
?>

<article <?php post_class('news-post match-height'); ?>>
  <a class="link-wrap" href="<?php the_permalink(); ?>">
    <h3 class="title"><?php the_title(); ?></h3>
    <div class="wrap-meta">
      <time class="updated meta" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time> |
      <span class="byline author vcard meta"><?= __('By ', 'sage'); ?><?= get_the_author(); ?></span>
    </div>
  </a>
  
  <?php if(!empty($feat_img_thumb)): ?>
  <a class="link-wrap img-wrap" href="<?php the_permalink(); ?>">
    <div class="video-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
    <img class="feat-img x-bg-cover lazyload-init" data-original="<?php echo $feat_img_thumb; ?>" alt="<?php the_title(); ?>">
  </a>
  <?php endif; ?>
  <div class="wrap-entry-post">
    <div class="desc">
      <?php the_content(); ?>
    </div>
    <div class="meta-post clearfix">
      <?php get_template_part('templates/share-buttons'); ?>
      <a class="comments-count" href="<?php the_permalink(); ?>">
        <i class="fa fa-comments" aria-hidden="true"></i> (<?php comments_number( '0', '1', '%' ); ?>)
      </a>
    </div>
  </div>
</article>
