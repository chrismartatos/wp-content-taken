<?php
  if(is_front_page() || is_archive()): endif;
    get_template_part('templates/elements/youtube-feed');


  echo do_shortcode('[otw_shortcode_widget_shortcode shortcode_type="widget_shortcode_tag_cloud" title="#TAGS" taxonomy="post_tag"][/otw_shortcode_widget_shortcode]');
?>

<div class="widget">
  <!--Footer info-->
  <?php
    $address = get_field('address','option');
    $email = get_field('email','option');
    $phone = get_field('phone','option');
  ?>
  <h2>STORE</h2>
  <div class="store-info">
    <div class="address"><?php echo $address; ?></div>
    <?php if($email): ?>
    <div class="email"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
    <?php endif; ?>
    <?php if($phone): ?>
    <div class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone; ?></div>
    <?php endif; ?>
  </div>

</div>
