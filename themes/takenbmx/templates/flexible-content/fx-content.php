<?php
if( have_rows('add_content_elements') ):

    while ( have_rows('add_content_elements') ) : the_row();

        /* Text editor
        ------------------------------------------------*/
        if( get_row_layout() == 'fx_row_text_editor_1' ): get_template_part('templates/flexible-content/elements-global/text_editor');


        /* Featured products
        ------------------------------------------------*/
        elseif( get_row_layout() == 'fx_featured_products' ): get_template_part('templates/flexible-content/elements-global/featured_products');

        /* Crew
        ------------------------------------------------*/
        elseif( get_row_layout() == 'fx_featured_products' ): get_template_part('templates/flexible-content/elements-global/add_crew');


        endif;//Close flexible-content rows layout

    endwhile;

else :

    // no flexible content

endif;

?>
