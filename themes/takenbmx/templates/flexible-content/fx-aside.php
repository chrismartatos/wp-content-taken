<?php

if( have_rows('add_sidebar') ):

    while ( have_rows('add_sidebar') ) : the_row();

        /* Text editor
        ------------------------------------------------*/
        if( get_row_layout() == 'fx_row_text_editor_1' ): get_template_part('templates/flexible-content/elements-global/text_editor');


        endif;//Close flexible-content rows layout

    endwhile;

else :

    // no flexible content

endif;

?>
