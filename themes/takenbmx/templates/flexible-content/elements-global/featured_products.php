<?php

//Defaults
$posts_per_page = "8";
$tag = "featured";

//get fields
$get_title = get_sub_field('title');
$get_items_count = get_sub_field('show_items');

//Get term
$get_tag = get_sub_field('category');
$get_tag_slug = $get_tag->slug;

//Set vars
if(!empty($get_items_count))
{
  $posts_per_page = $get_items_count;
}

if(!empty($get_tag_slug))
{
  $tag = $get_tag_slug;
}


//Args
$feat_args = array(
  'post_type' => 'product',
  'posts_per_page' => $posts_per_page,
  'ignore_sticky_posts'	=> 1,
  'post_status' => 'publish',
  'orderby' => 'date',
  'order' => 'ASC',
  'tax_query' =>
    array(
    'relation' => 'AND',
    array(
      'taxonomy' => 'product_tag',
      'field' => 'slug',
      'terms' => $tag,
    ),
  ),
);

//WP query
$feat_query = new WP_Query( $feat_args );
?>

<div class="fx-featured-products">
  <div class="container">

    <?php if($get_title): ?>
    <div class="row">
      <div class="col-lg-12">
        <h3 class="main-title"><?php echo $get_title; ?></h3>
      </div>
    </div>
    <?php endif; ?>

    <div class="row">
    <?php
    //Show featured Products
    if ( $feat_query->have_posts() )
    {
      while ( $feat_query->have_posts() )
      {
        $feat_query->the_post();

        get_template_part('templates/woocommerce-elements/featured-products-entry-meta');
      }
      wp_reset_postdata();
    }
    else
    {
      wc_get_template( 'loop/no-products-found.php' );
    }
    ?>
    </div>
  </div>
</div>
