<?php
//Tags - get params
$param_brand   = (isset($_GET['brand']))?urldecode($_GET['brand']): '';
$param_size   = (isset($_GET['size']))?urldecode($_GET['size']): '';
$param_color   = (isset($_GET['color']))?urldecode($_GET['color']): '';
$param_country = (isset($_GET['country']))?urldecode($_GET['country']): '';

//Price
$price_from = (isset($_GET['priceFrom']))?urldecode($_GET['priceFrom']): '';
$price_to   = (isset($_GET['priceTo']))?urldecode($_GET['priceTo']): '';

//Keywords
$keywords = (isset($_GET['keywords']))?urldecode($_GET['keywords']): '';

/*  TAGS
--------------------------------------------------*/
if(!empty($param_brand))
{
  $term_brand = get_term_by('slug', $param_brand, 'product_tag');

  echo '<span class="term-param">'.$term_brand->name.'</span>';
}

if(!empty($param_size))
{
  $term_size = get_term_by('slug', $param_size, 'product_tag');

  echo '<span class="term-param">'.$term_size->name.'</span>';
}

if(!empty($param_color))
{
  $term_color = get_term_by('slug', $param_color, 'product_tag');

  echo '<span class="term-param">'.$term_color->name.'</span>';
}

if(!empty($param_country))
{
  $term_country = get_term_by('slug', $param_country, 'product_tag');

  echo '<span class="term-param">'.$term_country->name.'</span>';
}

/*  Prices
--------------------------------------------------*/
if(!empty($price_from) || !empty($price_to))
{
  echo '<span class="term-param">$'.$price_from.' to $'.$price_to.'</span>';
}


/*  Keywords
--------------------------------------------------*/
if(!empty($keywords))
{
  echo '<span class="term-param"><b>Keywords:</b> '.$keywords.'</span>';
}
