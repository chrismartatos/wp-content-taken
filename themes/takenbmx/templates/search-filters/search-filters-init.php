<?php
  global $term;


  //Defaults
  $brand   = [];
  $size   = [];
  $color   = [];
  $country = [];
  $price   = [];
  $from    = [];
  $to      = [];

  //Tags - get params
  $param_brand   = (isset($_GET['brand']))? urldecode($_GET['brand']): '';
  $param_size   = (isset($_GET['size']))? urldecode($_GET['size']): '';
  $param_color   = (isset($_GET['color']))? urldecode($_GET['color']): '';
  $param_country = (isset($_GET['country']))? urldecode($_GET['country']): '';

  //Price
  $price_from = (isset($_GET['priceFrom']))? urldecode($_GET['priceFrom']): '';
  $price_to   = (isset($_GET['priceTo']))? urldecode($_GET['priceTo']): '';

  //Keywords
  $keywords = (isset($_GET['keywords']))? urldecode($_GET['keywords']): '';

  /*  TAGS
  --------------------------------------------------*/
  if(!empty($param_brand))
  {
    $brand = array("brand" => $param_brand);
  }

  if(!empty($param_size))
  {
    $size = array("size" => $param_size);
  }

  if(!empty($param_color))
  {
    $color = array("color" => $param_color);
  }

  if(!empty($param_country))
  {
    $country = array("country" => $param_country);
  }

  /*  Prices
  --------------------------------------------------*/
  if(!empty($price_from))
  {
    $from = array("priceFrom" => $price_from);
  }

  if(!empty($price_to))
  {
    $to = array("priceTo" => $price_to);
  }

  /*  Keywords
  --------------------------------------------------*/
  if(!empty($keywords))
  {
    $keywords = $keywords;
  }

  //Get custom field tag_filters and build tag filter menu
  $queried_object = get_queried_object();
  $taxonomy = $queried_object->taxonomy;
  $term_id = $queried_object->term_id;
  $term_name = $queried_object->name;


  //get terms for taxonomy and build menu
  $get_tags_brand = get_field('tag_filters_brand', $taxonomy . '_' . $term_id);
  $get_tags_color = get_field('tag_filters_color', $taxonomy . '_' . $term_id);
  $get_tags_size = get_field('tag_filters_size', $taxonomy . '_' . $term_id);
  $get_tags_prices = "on";

  $activate = (!empty($get_tags_brand) || !empty($get_tags_prices) || !empty($get_tags_color) || !empty($get_tags_color)) ? "activate" : "" ;
?>

<div class="product-filters-wrapper clearfix <?= $activate; ?>">

    <div id="toggle-mobile-menu">
      <div class="visible-sm visible-xs toggle-title">
        Filters
        <span class="open-ar fa fa-caret-right"></span>
      </div>

      <div class="toggle-menu">

        <nav id="search-filters-menu" class="filters-search-menu menu" data-href="<?= get_home_url(); ?>/product-category/<?= $term; ?>">
          <div class="inner clearfix">
            <!--Form is not being used -->
            <form action="#">
              <?php
              if( $get_tags_brand ):
              ?>
              <fieldset class="menu-item">
                <div class="wrap-select">
                  <select id="filter-brand" class="tag-filter-item select-filter" name="brand">
                    <option class="view-all" val="">Brands</option>
                    <?php
                      foreach( $get_tags_brand as $tag ):

                      $tag_id = $tag->term_id;
                      $tag_slug = $tag->slug;
                      $tag_name = $tag->name;
                      ?>
                      <option class="filter-mobile" data-val="<?= $tag_slug; ?>" data-param="brand" val="<?= "brand=".$tag_slug; ?>" id="option-<?= $tag_slug; ?>" data-id="<?= $tag_id; ?>">
                        <?= $tag_name; ?>
                      </option>
                      <?php
                      endforeach;
                    ?>
                  </select>
                </div>
              </fieldset>
              <?php endif; ?>

              <?php
              if( $get_tags_color ):
              ?>
              <fieldset class="menu-item">
                <div class="wrap-select">
                  <select id="filter-color" class="tag-filter-item select-filter" name="color">
                    <option class="view-all" val="">Colors</option>
                    <?php
                      foreach( $get_tags_color as $tag ):

                      $tag_id = $tag->term_id;
                      $tag_slug = $tag->slug;
                      $tag_name = $tag->name;
                      ?>
                      <option class="filter-mobile" data-val="<?= $tag_slug; ?>" data-param="color" val="<?= "color=".$tag_slug; ?>" id="option-<?= $tag_slug; ?>" data-id="<?= $tag_id; ?>">
                        <?= $tag_name; ?>
                      </option>
                      <?php
                      endforeach;
                    ?>
                  </select>
                </div>
              </fieldset>
              <?php endif; ?>

              <?php
              if( $get_tags_size ):
              ?>
              <fieldset class="menu-item">
                <div class="wrap-select">
                  <select id="filter-color" class="tag-filter-item select-filter" name="size">
                    <option class="view-all" val="">Sizes</option>
                    <?php
                      foreach( $get_tags_size as $tag ):

                      $tag_id = $tag->term_id;
                      $tag_slug = $tag->slug;
                      $tag_name = $tag->name;
                      ?>
                      <option class="filter-mobile" data-val="<?= $tag_slug; ?>" data-param="size" val="<?= "size=".$tag_slug; ?>" id="option-<?= $tag_slug; ?>" data-id="<?= $tag_id; ?>">
                        <?= $tag_name; ?>
                      </option>
                      <?php
                      endforeach;
                    ?>
                  </select>
                </div>
              </fieldset>
              <?php endif; ?>

		      <!--Show menu-->
		      <fieldset class="menu-item" style="display: none;">
		        <div class="wrap-select">
		          <select id="product-filter-prices" class="tag-filter-item select-price" name="price_filters">
		            <option class="view-all" val="">Τιμές</option>
		            <option class="price-range" id="option-025" data-from-param="priceFrom" data-from-val="0" data-to-param="priceTo" data-to-val="25" val="priceFrom=0&priceTo=25">€0 to €25</option>
		            <option class="price-range" id="option-2550" data-from-param="priceFrom" data-from-val="25" data-to-param="priceTo" data-to-val="50" val="priceFrom=25&priceTo=50">€25 to €50</option>
		            <option class="price-range" id="option-5075" data-from-param="priceFrom" data-from-val="50" data-to-param="priceTo" data-to-val="75" val="priceFrom=50&priceTo=75">€50 to €75</option>
		            <option class="price-range" id="option-75100" data-from-param="priceFrom" data-from-val="75" data-to-param="priceTo" data-to-val="100" val="priceFrom=75&priceTo=100">€75 to €100</option>
		            <option class="price-range" id="option-100150" data-from-param="priceFrom" data-from-val="100" data-to-param="priceTo" data-to-val="150" val="priceFrom=100&priceTo=150">€100 to €150</option>
		            <option class="price-range" id="option-150200" data-from-param="priceFrom" data-from-val="150" data-to-param="priceTo" data-to-val="200" val="priceFrom=150&priceTo=200">€150 to €200</option>
		            <option class="price-range" id="option-200250" data-from-param="priceFrom" data-from-val="200" data-to-param="priceTo" data-to-val="250" val="priceFrom=200&priceTo=250">€200 to €250</option>
		            <option class="price-range" id="option-250300" data-from-param="priceFrom" data-from-val="250" data-to-param="priceTo" data-to-val="300" val="priceFrom=250&priceTo=300">€250 to €300</option>
		            <option class="price-range" id="option-300350" data-from-param="priceFrom" data-from-val="300" data-to-param="priceTo" data-to-val="350" val="priceFrom=300&priceTo=350">€300 to €350</option>
		            <option class="price-range" id="option-350400" data-from-param="priceFrom" data-from-val="350" data-to-param="priceTo" data-to-val="400" val="priceFrom=350&priceTo=400">€350 to €400</option>
		            <option class="price-range" id="option-400600" data-from-param="priceFrom" data-from-val="400" data-to-param="priceTo" data-to-val="600" val="priceFrom=300&priceTo=350">€400 to €600</option>
		            <option class="price-range" id="option-600800" data-from-param="priceFrom" data-from-val="600" data-to-param="priceTo" data-to-val="800" val="priceFrom=300&priceTo=350">€600 to €800</option>

		          </select>
		        </div>
		      </fieldset>

              <!--SEARCH BUTTON-->
              <a id="filtering-init" style="display:none;" class="btn-primary" href="#">SEARCH</a>
          </form>
          </div><!--END .inner -->
        </nav><!--END .tag-filters-search-->

      </div><!--END .toggle-menu-->
    </div><!--END #toggle-mobile-menu-->

    <div id="tag-filter-results" class="clear">
     <div class="wrap-loader">
       <div class="css-loader"></div>
     </div>

     <?php

     /* FN: Filters init
     ---------------------------------------------------------------------*/

     // $Merge params array(except prices)
     $merge_tag_params = array_merge($brand + $size + $color + $country);

     // $Parameters
     $get_tag_params = array_values($merge_tag_params);

     if( function_exists( 'search_tag_filters_init' ) )
     {
       search_tag_filters_init($get_tag_params,$price_from,$price_to,$keywords,$term);
     }
     else
     {
       echo "<p>Oops...something went wrong. Please try again later or contact us.</p>";
     }
     ?>
    </div><!--END .tag-filter-results-->
</div>
