<?php
  /*
  ** Template Name: Shop Page
  */
?>
<div class="taken-shop">
  <div class="container">
    <?php
    if(is_cart()){
      echo '<h1 class="taken-title">'.get_the_title().'</h1>';
    }

    if(is_account_page() && is_user_logged_in()):
      echo '<h1 class="taken-title">'.get_the_title().'</h1>';
    endif;
    ?>
    <?php the_content(); ?>
  </div>
</div>
