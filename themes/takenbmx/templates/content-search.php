<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <?php if (get_post_type() === 'post') { get_template_part('templates/entry-meta'); } ?>
  </header>
  <div class="img" style="width: 200px; margin-bottom: 2rem;">
    <a href="<?php the_permalink(); ?>">
      <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" style="width: 100%;">
    </a>
  </div>
  <div class="entry-summary" style="padding-left: 1.5rem;">
    <?php the_excerpt(); ?>
  </div>
</article>
