<?php
  /*
  ** Template Name: Taken Kids
  */
?>

<?php get_template_part('templates/crew/crew-header'); ?>

<?php get_template_part('templates/flexible-content/fx-content'); ?>

<?php get_template_part('templates/crew/crew-kids'); ?>
