<?php
if ( is_product_category() )
{
  global $wp_query;

  $cat = $wp_query->get_queried_object();

  if(!empty($cat)):
  $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
  $image = wp_get_attachment_url( $thumbnail_id );

    if(!wp_is_mobile()):
      if ( $image ):
      ?>
      <div class="cat-img lazyload-init desktop-only" data-original="<?= $image; ?>">
        <?php do_action( 'woocommerce_archive_description' ); ?>
      </div>
      <?php
      endif;
    endif;
  endif;
}
?>

<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
  <header class="title-secondary">
    <h1 class="title"><?php woocommerce_page_title(); ?></h1>
  </header>
<?php endif; ?>

<?php
/**
 ***** Products filtering
 */
 get_template_part('templates/search-filters/search-filters-init');
?>

<?php
/**
 * woocommerce_after_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */

//*COMENTED because it breaks HTML*// do_action( 'woocommerce_after_main_content' );
?>
