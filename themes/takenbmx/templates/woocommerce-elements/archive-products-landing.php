<?php
  $args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'ignore_sticky_posts'	=> 1,
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'tax_query' => array(
  		'relation' => 'AND',
  		array(
  			'taxonomy' => 'product_cat',
  			'field'    => 'slug',
  			'terms'    =>  array('special-offers'),
  		),
  	),
  );

  $query = new WP_Query( $args );

  ?>
  <div id="products-page" class="wrap-kits">

    <?php
    //Header
    $cat = $query->get_queried_object();

    if(!empty($cat)):
      $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
      $image = wp_get_attachment_url( $thumbnail_id );
      $desc = term_description( $cat->term_id, 'product_cat');

      if(!wp_is_mobile()):
        if ( !empty($image) ):
        ?>
        <div class="cat-img bg-cover lazyload-init desktop-only" data-original="<?= $image; ?>">
          <?php
          if(!empty($desc)):
          echo '<div class="term-description">'.$desc.'</div>';
          endif;
          ?>
        </div>
        <?php
        endif;
      endif;
    endif;
    ?>

    <header class="title-secondary">
      <h1 class="title">NEW ARRIVALS</h1>
    </header>

    <div class="row">
      <?php
	  $arrivals_args = array(
	    'post_type' => 'product',
	    'posts_per_page' => 30,
	    'post_status' => 'publish',
	    'orderby' => 'date',
	    'order' => 'DESC',
	    'tax_query' => array(
	  		'relation' => 'AND',
	  		array(
	  			'taxonomy' => 'product_cat',
	  			'field'    => 'slug',
	  			'terms'    =>  array('new-arrivals'),
	  		),
	  	),
	  );

	  $arrivals = new WP_Query( $arrivals_args );
      if( $arrivals->have_posts() ):
        while ( $arrivals->have_posts() ): $arrivals->the_post();
          get_template_part('templates/woocommerce-elements/product-landing-entry-meta');
        endwhile;
        wp_reset_postdata();
      endif;
      ?>
    </div>

    <!--header class="title-secondary">
      <h1 class="title"><?php woocommerce_page_title(); ?> - SALES</h1>
    </header-->

    <!--div class="row">
    <?php
	/*
    if( $query->have_posts() ):
      while ( $query->have_posts() ): $query->the_post();
        //get_template_part('templates/woocommerce-elements/product-landing-entry-meta');
      endwhile;
      wp_reset_postdata();
    else:
      //wc_get_template( 'loop/no-products-found.php' );
    endif;*/
    ?>
    </div-->
  </div>
