<article class="col-sm-4 col-xs-6 col-md-3">
  <a class="item clear match-height" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
    <?php
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $sale = get_post_meta( get_the_ID(), '_sale_price', true);
    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'large', true);
    $feat_img = $image_url[0];

    if(!empty($sale))
    {
      echo '<span class="onsale">SALE</span>';
    }
    ?>
    <div class="feat-img bg-contain" style="background-image:url(<?php echo $feat_img; ?>)"></div>
    <h4 class="title"><?php the_title(); ?></h4>
    <div class="price">
      <?php
      if(empty($sale)):
        echo '$'.$price;
      else:
        echo '<del>$'.$price.'</del>  $'.$sale;
      endif;
      ?>
    </div>
  </a>
</article>
