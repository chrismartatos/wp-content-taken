<article class="col-sm-6 col-xs-12 col-md-6">
  <div class="item clearfix" style="margin-bottom: 4rem;">
    <?php
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $sale = get_post_meta( get_the_ID(), '_sale_price', true);
    $product = wc_get_product( get_the_ID() );
    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'large', true);
    $feat_img = $image_url[0];

    if ($product->is_type( 'simple' ))
    {
        $sale_price     =  $product->get_sale_price();
        $regular_price  =  $product->get_regular_price();
    }
    elseif($product->is_type('variable'))
    {
        $sale_price     =  $product->get_variation_sale_price( 'min', true );
        $regular_price  =  $product->get_variation_regular_price( 'max', true );
    }

    ?>
    <?php if(!empty($sale)): ?>
    <span class="onsale">Sale!</span>
    <?php endif; ?>
    <a href="<?php the_permalink(); ?>" class="feat-img bg-cover lazyload-init" data-original="<?= $feat_img; ?>"></a>
    <h4 class="title"><?php the_title(); ?></h4>
    <div class="desc" style="height:30px; font-size: 18px; font-weight: bold; overflow: hidden; margin-bottom: 1rem; ">
      <?php
      if(!empty($sale_price)){
        if($product->is_type('variable')){
          if($sale_price == $regular_price ){
            echo '';
          } else {
            echo '€'.$sale_price.' - ';
          }
        } else {
          echo '€'.$sale_price;
        }
      }

      if(!empty($regular_price))
      {
        echo '€'.$regular_price;
      }
      ?>
    </div>
    <a href="<?php the_permalink(); ?>" class="view-detail"><span style="font-weight: bold;">View Details</span> <i class="fa fa-chevron-right"></i></a>
  </div>
</article>
