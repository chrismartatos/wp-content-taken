<nav class="social-media-icons">
  <ul>
  <?php
  if(have_rows('add_social_media','option')):
    while(have_rows('add_social_media','option')): the_row();

      $title = get_sub_field('title');
      $href = get_sub_field('url');
      $icon = get_sub_field('choose_icon');

      echo '<li><a href="'.$href.'" title="'.$title.'" class="social-link" target="_blank"><i class="fa fa-'.$icon.'" aria-hidden="true"></i></a></li>';

    endwhile;
  endif;
  ?>
  </ul>
</nav>
