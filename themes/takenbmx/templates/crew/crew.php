<?php
$crew_args = array(
  'post_type' => 'crew',
  'posts_per_page' => '-1',
  'ignore_sticky_posts'	=> 1,
  'post_status' => 'publish',
  'orderby' => 'date',
  'order' => 'ASC',
  'tax_query' => array(
		array(
			'taxonomy' => 'crew_category',
			'field'    => 'slug',
			'terms'    => 'takenboys',
		),
	),
);

$crew_query = new WP_Query( $crew_args );
?>

<div id="taken-crew" class="wrap-taken-crew container-fluid">

  <nav id="crew-nav" class="categories-nav">
    <header class="title-secondary">
      <h1 class="title"><?= get_the_title(); ?></h1>
    </header>
    <div class="categories-nav-wrap clearfix">
      <ul>
      <?php
	  if ( $crew_query->have_posts() ):
        while ( $crew_query->have_posts() ):
          $crew_query->the_post();
          $_id = sanitize_title(get_the_title());

          echo '<li><a class="scroll-to-crew" href="#'.$_id.'" data-offset="80">'.get_the_title().'</a></li>';
        endwhile; wp_reset_postdata();
      endif;
      ?>
      </ul>
    </div>
  </nav>
  
  <div class="clearfix">
  <?php
  if ( $crew_query->have_posts() ):
	  while ( $crew_query->have_posts() ):
	    $crew_query->the_post();
	    get_template_part('templates/crew/crew-meta');
	  endwhile; wp_reset_postdata();
  endif;
  ?>
  </div>
</div>
