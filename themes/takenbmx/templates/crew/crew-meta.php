<?php
$gallery = get_field('gallery');
$latest_edit = get_field('latest_edit');
$_id = sanitize_title(get_the_title());
?>
<article id="<?= $_id; ?>" class="clearfix crew-member">

  <div class="wrap-left item">
    <div class="clearfix">
    <?php
    if( $gallery ): ?>
      <div class="gallery clearfix">
        <?php foreach( $gallery as $image ):

          $url = $image['sizes']['large'];
          $cpt = $image['caption'];
          $alt = $image['alt'];
          ?>
          <figure class="slide-item">
            <a href="<?= $url; ?>" class="swipebox img-item bg-cover lazy-load-slide" rel="<?php the_title(); ?>" data-original="<?= $url; ?>">
              <?php if(!empty($cpt)): ?>
                <figcaption class="caption">
                  <?= $cpt; ?>
                </figcaption>
              <?php endif; ?>
            </a>
          </figure>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
    </div>
  </div>

  <div class="wrap-right item">
    <div class="clearfix">
    <header class="title-secondary">
      <h2 class="title"><?php the_title(); ?></h2>
    </header>
    <div class="content"><?php the_content(); ?></div>

    <?php if(!empty($latest_edit)): ?>
    <div class="latest-edit">
      <a href="<?= $latest_edit; ?>" rel="youtube" class="swipebox">
        <span>View Latest Web Edit <i class="fa fa-play-circle-o" aria-hidden="true"></i></span>
      </a>
    </div>
    <?php endif; ?>

    <?php
    if(have_rows('add_social_media')):
      ?>
      <nav class="social-media-icons clearfix">
        <ul>
          <?php
          while(have_rows('add_social_media')): the_row();

            $title = get_sub_field('title');
            $href = get_sub_field('url');
            $icon = get_sub_field('choose_icon');

            echo '<li><a href="'.$href.'" title="'.$title.'" class="social-link" target="_blank"><i class="fa fa-'.$icon.'" aria-hidden="true"></i></a></li>';

          endwhile;
          ?>
        </ul>
      </nav>
      <?php
    endif;
    ?>
    </div>
  </div>

</article>
