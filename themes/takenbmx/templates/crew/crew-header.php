<?php 
  $image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', true);
  $feat_img = $image_url_full[0];
?>
<div class="crew-header bg-cover bg-fixed" style="background-image:url(<?= $feat_img; ?>);">

  <a class="scroll-to-crew wrap-scroll" href="#taken-crew" data-offset="110">
		<div class="text"><?php the_field('keep_scrolling_text'); ?></div>
		<span class="scroll-arrows one"></span>
		<span class="scroll-arrows two"></span>
		<span class="scroll-arrows three"></span>
	</a>

</div>