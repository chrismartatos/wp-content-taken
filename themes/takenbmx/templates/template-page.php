<?php
  /*
  ** Template Name: Blank Page
  */
?>
<div id="taken-page" class="container">
  <h1 class="taken-title"><?= the_title(); ?></h1>
  <?php the_content(); ?>
</div>
