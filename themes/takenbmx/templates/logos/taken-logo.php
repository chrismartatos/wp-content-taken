<?php $logo_header = get_field('image','option'); ?>

<!-- Logo -->
<a class="takenbmx-logo" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo( 'name' ); ?>">
  <img src="<?php echo $logo_header['url']; ?>" alt="<?php bloginfo( 'name' ); ?>">
</a>
